## Blue Maestro Utility App

An app to connect to Blue Maestro BLE devices via UART for Android. The app allows users to send commands to the device and graph data, if possible for the device.

The app can download the device's logs and store them in a database for later viewing, as a graph, table, or exported CSV file.

### Compile

The project can be compiled using Android Studio and Gradle

### Information

Version: 2.0.1

Android: Version 4.3 or above
