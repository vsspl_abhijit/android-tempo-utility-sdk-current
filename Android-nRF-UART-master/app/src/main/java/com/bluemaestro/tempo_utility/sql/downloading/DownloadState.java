package com.bluemaestro.tempo_utility.sql.downloading;

/**
 * Created by Garrett on 25/08/2016.
 *
 * The download state after attempting to download logs
 *
 * This can be:
 *  - SUCCESS:
 *      All logs were able to be downloaded from the device
 *  - FAILURE_NOT_DEFINED:
 *      Logs were not able to be downloaded from the device;
 *      the device does not support downloading
 *  - FAILURE_DISCONNECT:
 *      Logs were not able to be downloaded from the device;
 *      the device disconnected from the app midway
 */
public enum DownloadState {
    SUCCESS,
    FAILURE_NOT_DEFINED,
    FAILURE_DISCONNECT
}
