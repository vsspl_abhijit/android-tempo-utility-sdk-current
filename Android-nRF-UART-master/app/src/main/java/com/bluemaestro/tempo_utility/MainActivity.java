
/*
 * Copyright (c) 2016, Nordic Semiconductor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.bluemaestro.tempo_utility;




import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;


import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import android.bluetooth.BluetoothManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bluemaestro.tempo_utility.ble.ScanRecordParser;
import com.bluemaestro.tempo_utility.ble.Utility;
import com.bluemaestro.tempo_utility.devices.BMDevice;
import com.bluemaestro.tempo_utility.devices.BMDeviceMap;
import com.bluemaestro.tempo_utility.sql.BMDatabase;
import com.bluemaestro.tempo_utility.sql.downloading.BMDownloader;
import com.bluemaestro.tempo_utility.sql.downloading.DownloadState;
import com.bluemaestro.tempo_utility.views.dialogs.BMAlertDialog;
import com.bluemaestro.tempo_utility.views.dialogs.BMProgressIndicator;
import com.bluemaestro.tempo_utility.views.graphs.BMLineChart;

import static android.R.id.message;
import static java.lang.Thread.sleep;

public class MainActivity extends Activity implements RadioGroup.OnCheckedChangeListener {

    public static final String TAG = "BlueMaestro";

    private static final int REQUEST_SELECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int SELECT_STORED_DATA = 3;
    private static final int REQUEST_COMMAND = 5;
    private static final int UART_PROFILE_READY = 10;

    private static final int UART_PROFILE_CONNECTED = 20;
    private static final int UART_PROFILE_DISCONNECTED = 21;
    private static final int STATE_OFF = 10;

    private volatile boolean unstableBluetooth = false;
    private volatile boolean TXNotificationIsEnabled = false;

    private Boolean automaticDownload = false;

    //For commands to device
    private Boolean commandMode = false; //Whether trying to pass a command
    private String command; //The actual command to pass
    private Boolean commandSuccess = false; //Whether a command was actually passed or not
    private String commandResponse; //The device's response to the command

    public BMAlertDialog dialogCommand, dialogCommencingDownload, dialogStart, dialogStoredData;
    public BMProgressIndicator progressIndicatorDownload, progressIndicatorDatabase, progressIndicatorGraph;

    private int mState = UART_PROFILE_DISCONNECTED;

    private UartService mService = null;

    private BluetoothDevice mDevice = null;
    private BMDevice mBMDevice = null;
    private BMDatabase mBMDatabase = null;
    private BluetoothAdapter mBtAdapter = null;
    private String selectedDeviceAddress = null;
    private List<BluetoothDevice> deviceList;

    // Console
    private ListView messageListView;
    private ArrayAdapter<String> listAdapter;

    // Buttons
    private Button
            btnConnectDisconnect, btnDownload, btnStoredData,
            btnSend, btnGraph, btnTable, btnExport, btnStream, btnSettings, btnSelect;

    // Command line
    private EditText edtMessage;

    // Graph
    private BMLineChart lineChart;

    // Table
    private ListView table;

    // View Group for devices 22 and 23
    private ViewGroup viewFor22And23;

    // VIew Group for device 27
    private ViewGroup viewFor27;

    // View Group for devices 13
    private ViewGroup viewFor13;

    // View Group for device 32
    private ViewGroup viewFor32;

    //View Group for device 42
    private ViewGroup viewFor42;

    //View Group for device 52
    private ViewGroup viewFor52;

    //View Group for device 62
    private ViewGroup viewFor62;

    private enum TypeOfView{
        INTRODUCTION,
        DEVICE_LIST,
        DEVICE_DETAILS,
        GRAPH,
        TABLE,
        STORED_DATA_GRAPH,
        STORED_DATA_TABLE
    }

    private enum TypeOfGraph{
        BURST,
        HISTORY,
        NONE
    }

    public TypeOfGraph graphType;

    public TypeOfView currentContentView;

    public void setCurrentContentView(TypeOfView view) {
        this.currentContentView = view;
    }

    public TypeOfView getCurrentView() {
        return  currentContentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.start_screen);
        setCurrentContentView(TypeOfView.INTRODUCTION);

        View rootView = findViewById(R.id.introduction_layout).getRootView();
        StyleOverride.setDefaultFont(rootView, this, "Montserrat-Regular.ttf");

        final TextView rateApp = (TextView) findViewById(R.id.rate_app_text);
        rateApp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
               rateApp();
            }
        });

        // Create custom Blue Maestro action bar
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

        // Inflate custom action bar
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View title = inflater.inflate(R.layout.title_bar, null);
        TextView version = (TextView) title.findViewById(R.id.version);
        version.setText("Version: 3.02.02");
        actionBar.setCustomView(title);

        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBtAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        setUpButtonsAndCallHandlersForIntroduction();
        service_init();

    }

    public void rateApp()
    {
        try
        {
            Intent rateIntent = rateIntentForUrl("market://details");
            startActivity(rateIntent);
        }
        catch (ActivityNotFoundException e)
        {
            Intent rateIntent = rateIntentForUrl("https://play.google.com/store/apps/details");
            startActivity(rateIntent);
        }
    }

    private Intent rateIntentForUrl(String url)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, getPackageName())));
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
        if (Build.VERSION.SDK_INT >= 21)
        {
            //flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        }
        else
        {
            //noinspection deprecation
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }
        intent.addFlags(flags);
        return intent;
    }


    //This is the branding screen, with only two buttons
    private void setUpButtonsAndCallHandlersForIntroduction() {

        btnSelect = (Button) findViewById(R.id.select_device_button);
        btnStoredData = (Button) findViewById(R.id.stored_data_button);
        // Handle History button
        btnStoredData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickStoredData();
            }
        });

        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onClickSelectDevice();
            }
        });

    }

    //This is device details screen, with three buttons.  Download, graph and settings.
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");

        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(UARTStatusChangeReceiver);
        } catch (Exception ignore) {
            Log.e(TAG, ignore.toString());
        }
        unbindService(mServiceConnection);
        if(mService != null){
            mService.stopSelf();
            mService = null;
        }
        if(mBMDatabase != null) mBMDatabase.close();
        BMDeviceMap.INSTANCE.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if (!mBtAdapter.isEnabled()) {
            Log.i(TAG, "onResume - BT not enabled yet");
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_SELECT_DEVICE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    selectedDeviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
                    mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(selectedDeviceAddress);
                    mBMDevice = BMDeviceMap.INSTANCE.getBMDevice(mDevice.getAddress());
                    mBMDatabase = BMDeviceMap.INSTANCE.getBMDatabase(mBMDevice.getAddress());
                    Log.d(TAG, "... onActivityResultdevice.address==" + mDevice + "mserviceValue" + mService);
                    mBMDevice.setDownloadReady(true);
                    setUpDeviceDetailView();
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(this, "Bluetooth has turned on ", Toast.LENGTH_SHORT).show();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, "Problem in BT Turning ON ", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            case SELECT_STORED_DATA:
                // When the StoredDataActivity return, with the selected device address
                if (resultCode == Activity.RESULT_OK && data != null) {
                    final String deviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
                    mBMDatabase = BMDeviceMap.INSTANCE.createReadableBMDatabase(this, deviceAddress);
                    dialogCommand = new BMAlertDialog(MainActivity.this,
                            "",
                            "Communicating command with " + mBMDatabase.getAddress() + "...");
                    dialogCommand.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            if (commandSuccess) {
                                showAlert("Response from device is " + commandResponse);
                            } else {
                                showAlert("Failed to communicate command with " + mBMDatabase.getAddress());
                            };
                        }
                    });
                    dialogCommand.show();
                    dialogCommand.applyFont(MainActivity.this, "Montserrat-Regular.ttf");
                    /*
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                             progressIndicatorDatabase = new BMProgressIndicator(MainActivity.this,
                                    "",
                                    "Extracting stored data for \n" + mBMDatabase.getAddress() + "...");
                            progressIndicatorDatabase.setMax(mBMDatabase.getDataCount());
                            progressIndicatorDatabase.setNegativeButton();
                            progressIndicatorDatabase.showProgressDialog();
                            progressIndicatorDatabase.applyFont(MainActivity.this, "Montserrat-Regular.ttf");

                        }
                    });
                    */

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onSelectStoredDataDevice(deviceAddress, dialogCommand);
                        }
                    }, 500);


                }
                break;
            case REQUEST_COMMAND:
                //When Commands List returns with selected command
                if (resultCode == Activity.RESULT_OK && data != null) {
                    command = data.getStringExtra("Command");
                    Log.d(TAG, "MainActivity - Command Returned is :" +command);
                    mService.connect(selectedDeviceAddress);
                    if (command.matches("console")) {

                        consoleMode();
                    } else {
                        commandMode = true;
                        commandSuccess = false;
                    }

                }
                break;

            default:
                Log.e(TAG, "wrong request code");
                break;
        }
    }

    /************ CREATES NEW VIEW WITH DATA FROM DEVICE ************/

    private void setUpDeviceDetailView() {

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        setCurrentContentView(TypeOfView.DEVICE_DETAILS);


        // Create custom Blue Maestro action bar
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

        // Inflate custom action bar
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View title = inflater.inflate(R.layout.title_bar_back, null);
        actionBar.setCustomView(title);

        Log.e(TAG, "Device version is :" + mBMDevice.getVersion());

        if ((mBMDevice.getVersion() == 13) || (mBMDevice.getVersion() == 113)) {
            setContentView(R.layout.main_v13);
            lineChart = (BMLineChart) findViewById(R.id.lineChart);
            viewFor13 = (ViewGroup) findViewById(R.id.view_for_v13);
            mBMDevice.updateViewGroupForDetails(viewFor13);
        } else if (mBMDevice.getVersion() == 32) {
            setContentView(R.layout.main_v32);
            lineChart = (BMLineChart) findViewById(R.id.lineChart);
            viewFor32 = (ViewGroup) findViewById(R.id.view_for_v32);
            mBMDevice.updateViewGroupForDetails(viewFor32);
        } else if (mBMDevice.getVersion() == 27) {
            setContentView(R.layout.main_v27);
            viewFor27 = (ViewGroup) findViewById(R.id.view_for_v27);
            mBMDevice.updateViewGroupForDetails(viewFor27);
        } else if (mBMDevice.getVersion() == 42) {
            setContentView(R.layout.main_v42);
            viewFor42 = (ViewGroup) findViewById(R.id.view_for_v42);
            lineChart = (BMLineChart) findViewById(R.id.lineChart);
            mBMDevice.updateViewGroupForDetails(viewFor42);
        }  else if (mBMDevice.getVersion() == 52) {
            setContentView(R.layout.main_v27);
            viewFor52 = (ViewGroup) findViewById(R.id.view_for_v52);
            mBMDevice.updateViewGroupForDetails(viewFor27);
        } else if (mBMDevice.getVersion() == 62) {
            setContentView(R.layout.main_v27);
            viewFor62 = (ViewGroup) findViewById(R.id.view_for_v62);
            mBMDevice.updateViewGroupForDetails(viewFor27);
        } else {
            setContentView(R.layout.main_v23);
            lineChart = (BMLineChart) findViewById(R.id.lineChart);
            viewFor22And23 = (ViewGroup) findViewById(R.id.view_for_v23);
            mBMDevice.updateViewGroupForDetails(viewFor22And23);
        }
        table = (ListView) findViewById(R.id.table_listView);

        //Make Buttons live
        btnDownload = (Button) findViewById(R.id.downloadButton);
        btnGraph = (Button) findViewById(R.id.graphButton);
        btnSettings = (Button) findViewById(R.id.settingsButton);

        //Set click listener for btnSettings
        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSettings();
            }
        });

        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDownload();
            }
        });

        btnGraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickGraph();
            }
        });
    }

    private void consoleMode() {
        setContentView(R.layout.main);
        graphType = TypeOfGraph.NONE;
        btnConnectDisconnect = (Button) findViewById(R.id.btn_select);
        if (mState == UART_PROFILE_CONNECTED) btnConnectDisconnect.setText("Disconnect");
        btnConnectDisconnect.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onClickConnectDisconnect();
            }
        });
        btnSend = (Button) findViewById(R.id.sendButton);

        btnSend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onClickSend();
            }
        });
        btnStream = (Button) findViewById(R.id.graphButton);
        btnStream.setEnabled(true);
        btnStream.setOnClickListener (new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    onClickBurstGraph();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });


        edtMessage = (EditText) findViewById(R.id.sendText);
        edtMessage.getText().clear();
        edtMessage.setVisibility(View.VISIBLE);

        messageListView = (ListView) findViewById(R.id.listMessage);
        listAdapter = new CustomArrayAdapter<String>(this, R.layout.message_detail);
        messageListView.setAdapter(listAdapter);
        messageListView.setDivider(null);
        messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
        edtMessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionID, KeyEvent event) {
            if(actionID !=EditorInfo.IME_ACTION_SEND)return false;
            onClickSend();
            return true;
        }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                if (((getCurrentView()!=TypeOfView.GRAPH) || (getCurrentView()!=TypeOfView.STORED_DATA_GRAPH)) && (graphType !=TypeOfGraph.BURST)) {
                    if (mState == UART_PROFILE_CONNECTED) mService.disconnect();
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    if (dialogCommand != null) {
                        dialogCommand.dismiss();
                    }
                    if (dialogStart != null) {
                        dialogStart.dismiss();
                    }

                } else if (getCurrentView()==TypeOfView.STORED_DATA_GRAPH){

                } else if (graphType == TypeOfGraph.BURST) {
                    consoleMode();
                }

                else {
                    Log.d(TAG, "currentlyOnGraph");
                    setUpDeviceDetailView();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /************************** INITIALISE **************************/

    private void service_init() {
        Intent bindIntent = new Intent(this, UartService.class);
        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
        LocalBroadcastManager.getInstance(this).registerReceiver(UARTStatusChangeReceiver, makeGattUpdateIntentFilter());
    }

    /************************** UART STATUS CHANGE **************************/

    // UART service connected/disconnected
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            mService = ((UartService.LocalBinder) rawBinder).getService();
            Log.d(TAG, "onServiceConnected mService= " + mService);
            if (!mService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
        }

        public void onServiceDisconnected(ComponentName classname) {
            Log.d(TAG, "onService Disconnect");
            if(mService != null) mService.disconnect();
            mService = null;
        }
    };

    // Main UART broadcast receiver
    private final BroadcastReceiver UARTStatusChangeReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(UartService.ACTION_GATT_CONNECTED)) {
                onGattConnected();
            } else if (action.equals(UartService.ACTION_GATT_DISCONNECTED)) {
                onGattDisconnected();
            } else if (action.equals(UartService.ACTION_GATT_SERVICES_DISCOVERED)) {
                onGattServicesDiscovered();
            } else if (action.equals(UartService.ACTION_DATA_AVAILABLE)) {
                onDataAvailable(intent.getByteArrayExtra(UartService.EXTRA_DATA));
            } else if (action.equals(UartService.DEVICE_DOES_NOT_SUPPORT_UART)) {
                onDeviceDoesNotSupportUART();
            } else if (action.equals(UartService.UART_TX_ENABLED)) {
                onUARTTXEnabled();
            } else{

            }
        }
    };

    private void onGattConnected() {
        runOnUiThread(new Runnable() {
            public void run() {
                String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                Log.d(TAG, "UART_CONNECT_MSG");
                unstableBluetooth = true;
                mState = UART_PROFILE_CONNECTED;
                if (!automaticDownload && !commandMode && (btnConnectDisconnect != null)) {
                    btnConnectDisconnect = (Button) findViewById(R.id.btn_select);
                    btnConnectDisconnect.setText("Disconnect");
                    btnStoredData.setVisibility(View.GONE);
                    btnDownload.setVisibility(View.VISIBLE);
                    btnDownload.setEnabled(mBMDatabase != null);
                    edtMessage.setEnabled(true);
                    btnSend.setEnabled(true);
                    btnGraph.setEnabled(true);
                    //lineChart.clear();
                    ((TextView) findViewById(R.id.deviceName)).setText(mDevice.getName() + " - ready");
                    listAdapter.add("Connected to: " + mDevice.getName() + " (" + mDevice.getAddress() + ")");
                    messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
                }

            }
        });
    }

    private void onGattDisconnected() {
        runOnUiThread(new Runnable() {
            public void run() {
                String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                Log.d(TAG, "UART_DISCONNECT_MSG");
                if (!automaticDownload && !commandMode && (btnConnectDisconnect != null)) {

                    btnConnectDisconnect.setText("Connect");
                    btnStoredData.setVisibility(View.VISIBLE);
                    btnDownload.setVisibility(View.GONE);
                    //edtMessage.setEnabled(false);
                    //btnSend.setEnabled(false);
                    //btnGraph.setEnabled(false);
                    TextView deviceName = (TextView) findViewById(R.id.deviceName);
                    if (deviceName != null) deviceName.setText("Not Connected");
                    listAdapter.add("Disconnected from: " + mDevice.getName() + " (" + mDevice.getAddress() + ")");
                    listAdapter.notifyDataSetChanged();
                    mState = UART_PROFILE_DISCONNECTED;
                    messageListView.setSelection(listAdapter.getCount() - 1);
                }
                mService.close();
                automaticDownload = false;
                commandMode = false;
            }
        });
    }

    private boolean scanLeDevice(final boolean enable) {

        // Stops scanning after a pre-defined scan period.
        boolean success = mBtAdapter.startLeScan(mLeScanCallback);
        return success;
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    addDevice(device, rssi, scanRecord);
                }
            });
        }
    };

    private void addDevice(BluetoothDevice device, int rssi, byte[] scanRecord) {
        boolean deviceFound = false;

        if (device.getAddress() == mBMDevice.getAddress()) {
            //Log.d("ScanRecordParser", "Scan Record data: 0x" + Utility.bytesAsHexString(scanRecord));
            ScanRecordParser parser = null;
            try {
                parser = new ScanRecordParser(scanRecord);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            scanLeDevice(false);

        }
    }

    private void onGattServicesDiscovered(){

        unstableBluetooth = false;
        Log.d(TAG, "ENABLETXNOTIFICATION");
        if (automaticDownload) {
            download();
        }
        if (commandMode) {
            mService.enableTXNotification();  //Is this right?
            processCommand();
        }
    }

    private void onUARTTXEnabled(){
        if (automaticDownload) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            download();
        }
    }

    /*  Dormant method only used for checking throughput of the peripheral
    private void onDataAvailable(final byte[] txValue) {
        Log.d(TAG, "DATAAVAILABLE");
        Log.d(TAG, "Received message is : " + txValue);
        //dataProcessor(txValue);
    }
    */

    private void onDataAvailable(final byte[] txValue) {
        Log.d(TAG, "DATAAVAILABLE");
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    String text = new String(txValue, "UTF-8").trim();
                    Log.d(TAG, "Received message is : " + text);
                    if (commandMode) {
                        commandResponse = text;
                        commandSuccess = true;
                        dialogCommand.dismiss();
                        showAlert("Response from device is : \n\n" +commandResponse);
                        mBMDevice.updateChart(lineChart, text);
                    } else {
                        String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                        listAdapter.add(text);
                        listAdapter.notifyDataSetChanged();
                        if (messageListView.getVisibility() == View.VISIBLE) {
                            messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
                        } else {
                            messageListView.setSelection(listAdapter.getCount() - 1);
                        }

                        if (mBMDevice == null) return;
                        mBMDevice.updateChart(lineChart, text);

                        if (mBMDatabase == null) return;
                        //mBMDatabase.addData(BMDatabase.TIMESTAMP_NOW(), text);
                    }

                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }

    private void onDeviceDoesNotSupportUART() {
        showMessage("Device doesn't support UART. Disconnecting");
        mService.disconnect();
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UartService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(UartService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(UartService.DEVICE_DOES_NOT_SUPPORT_UART);
        intentFilter.addAction(UartService.UART_TX_ENABLED);
        return intentFilter;
    }

    /************************** BUTTON CLICK HANDLERS **************************/

    private void onClickSelectDevice(){
        if (!mBtAdapter.isEnabled()) {
            Log.i(TAG, "onClick - BT not enabled yet");
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else {
                // Connect button pressed, open DeviceListActivity class, with popup windows that scan for devices
                Intent newIntent = new Intent(MainActivity.this, DeviceListActivity.class);
                startActivityForResult(newIntent, REQUEST_SELECT_DEVICE);
        }
    }

    private void onClickConnectDisconnect(){
            if (btnConnectDisconnect.getText().equals("Connect")) {
                // Connect button pressed, open DeviceListActivity class, with popup windows that scan for devices
                edtMessage.getText().clear();
                edtMessage.setVisibility(View.VISIBLE);
                messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
                mService.connect(selectedDeviceAddress);
            } else {
                // Send command to device to disconnect on its side
                if (mService!=null) {
                    try {
                        Utility.sendCommand(mService, "*qq");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                // Disconnect button pressed
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (mDevice != null && mService !=null) mService.disconnect();
                //mBMDevice.setDownloadReady(true);
                if (lineChart != null) {
                    lineChart.setVisibility(View.GONE);
                }
                messageListView.setVisibility(View.VISIBLE);
                messageListView.setSelection(listAdapter.getCount() - 1);
            }
    }

    private void onClickDownload(){
        //mService.connect(selectedDeviceAddress);
        //Download method is called once the services have been discovered
        //and this flag is set
        mService.connect(selectedDeviceAddress);
        automaticDownload = true;

        progressIndicatorDownload = new BMProgressIndicator(MainActivity.this,
                "",
                "Downloading logs from " + mBMDatabase.getAddress() +" and assigning" +
                        " each log a time stamp.  Time stamping may take a while, please wait...");
        //if (progressIndicatorDownload != null) {progressIndicatorDownload.dismiss();}
        progressIndicatorDownload.setNegativeButton();
        progressIndicatorDownload.showProgressDialog();
        progressIndicatorDownload.preventCancelOnClickOutside();
        progressIndicatorDownload.applyFont(MainActivity.this, "Montserrat-Regular.ttf");
        int startValue = mBMDevice.getLogCount();
        progressIndicatorDownload.setMax(startValue);
    }

    private void onClickStoredData(){

        Intent newIntent = new Intent(MainActivity.this, StoredDataActivity.class);
        dialogStart = new BMAlertDialog(MainActivity.this,
                "",
                "Finding databases, please wait...");
        dialogStart.show();
        startActivityForResult(newIntent, SELECT_STORED_DATA);
    }

    private void onClickSend(){
        String message = edtMessage.getText().toString().trim();

        // Close keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtMessage.getWindowToken(), 0);

        byte[] value;
        try {
            // Send data to service
            if (mService == null || mState == UART_PROFILE_DISCONNECTED) return;
            Utility.sendCommand(mService, message);
            // Update the log with time stamp
            String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
            listAdapter.add(message);
            listAdapter.notifyDataSetChanged();
            messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
            edtMessage.getText().clear();

            //lineChart.setVisibility(View.GONE);
            //messageListView.setVisibility(View.VISIBLE);
            messageListView.setSelection(listAdapter.getCount() - 1);

            if(mBMDevice == null) return;
            mBMDevice.processCommand(message);
            //mBMDevice.setupChart(lineChart, message);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void onClickBurstGraph() throws UnsupportedEncodingException {
        if (graphType != TypeOfGraph.BURST && mBMDevice != null && mService != null && mState != UART_PROFILE_DISCONNECTED) {
            setContentView(R.layout.graph_screen);
            setCurrentContentView(TypeOfView.GRAPH);
            lineChart = (BMLineChart) findViewById(R.id.lineChart);
            lineChart.setVisibility(View.VISIBLE);
            btnGraph = (Button) findViewById(R.id.graph_table_Button);
            btnExport = (Button) findViewById(R.id.graph_export_Button);
            btnGraph.setVisibility(View.GONE);
            btnExport.setVisibility(View.GONE);
            //String command = "*bur";
            String message = "*bur";
            Utility.sendCommand(mService, message);
            mBMDevice.setupChart(lineChart, message);
            graphType = TypeOfGraph.BURST;
        } else {
            return;
        }
        //mBMDevice.setupChart(lineChart, command);

    }

    private void onClickGraph(){
        Log.d("MainActivity - History", "Address: " + selectedDeviceAddress);
        //setContentView(R.layout.graph_screen);
        //mBMDatabase = BMDeviceMap.INSTANCE.createReadableBMDatabase(this, selectedDeviceAddress);
        mBMDatabase = BMDeviceMap.INSTANCE.getBMDatabase(selectedDeviceAddress);
        Log.d("MainActivity - History", "Name: " + mBMDatabase.getName() + " | Version: " + mBMDatabase.getVersion());
                setContentView(R.layout.graph_screen);
                setCurrentContentView(TypeOfView.GRAPH);
                graphType = TypeOfGraph.HISTORY;
                ActionBar actionBar = getActionBar();
                actionBar.setDisplayHomeAsUpEnabled(true);

                lineChart = (BMLineChart) findViewById(R.id.lineChart);

                // Create custom Blue Maestro action bar
                actionBar.setDisplayShowCustomEnabled(true);
                actionBar.setDisplayShowTitleEnabled(false);

                // Inflate custom action bar
                LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                View title = inflater.inflate(R.layout.graph_title_bar_back, null);
                actionBar.setCustomView(title);

                lineChart = (BMLineChart) findViewById(R.id.lineChart);
                lineChart.setVisibility(View.VISIBLE);
                if (viewFor22And23 != null) viewFor22And23.setVisibility(View.GONE);
                if (viewFor13 != null) viewFor13.setVisibility(View.GONE);
                if (viewFor27 != null) viewFor27.setVisibility(View.GONE);
                if (viewFor32 != null) viewFor32.setVisibility(View.GONE);
                if (viewFor42 != null) viewFor42.setVisibility(View.GONE);

                showChartForDevice();

                Log.d(TAG, "Graph visible");
                lineChart.setPinchZoom(true);
                lineChart.setHorizontalScrollBarEnabled(true);
                final Button btnTableGraph = (Button) findViewById(R.id.graph_table_Button);
                Button btnExportGraph = (Button) findViewById(R.id.graph_export_Button);
                btnTableGraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickTable(btnTableGraph);
            }
        });
                btnExportGraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickExport();
            }
        });


    }

    private void onSelectStoredDataDevice(final String deviceAddress, BMAlertDialog dialog){

        if (dialog != null) dialog.dismiss();
        setContentView(R.layout.graph_screen);


        setContentView(R.layout.graph_screen);
        setCurrentContentView(TypeOfView.STORED_DATA_GRAPH);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Create custom Blue Maestro action bar
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

        // Inflate custom action bar
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View title = inflater.inflate(R.layout.graph_title_bar_back, null);

        actionBar.setCustomView(title);

        lineChart = (BMLineChart) findViewById(R.id.lineChart);
        lineChart.setVisibility(View.VISIBLE);

        showChartForDevice();
        TextView graph_back_text = (TextView) findViewById(R.id.title_graph_bar_back);
        graph_back_text.setText("Back to Select Device");

        Log.d(TAG, "Graph visible");
        if (dialogStoredData!=null) dialogStoredData.dismiss();
        lineChart.setPinchZoom(true);
        lineChart.setHorizontalScrollBarEnabled(true);
        final Button btnTableGraph = (Button) findViewById(R.id.graph_table_Button);
        Button btnExportGraph = (Button) findViewById(R.id.graph_export_Button);
        btnTableGraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickTable(btnTableGraph);
            }
        });
        btnExportGraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickExport();
            }
        });
    }


    private void onClickTable(Button tableGraphButton){
        table = (ListView) findViewById(R.id.table_listView);
        switch(table.getVisibility()){
            case View.VISIBLE:
                findViewById(R.id.tableViewHeading).setVisibility(View.GONE);
                table.setVisibility(View.GONE);
                lineChart.setVisibility(View.VISIBLE);
                tableGraphButton.setText("Table");
                //messageListView.setVisibility(View.VISIBLE);
                Log.d(TAG, "Table now gone, chart visible");
                break;
            case View.GONE:
                findViewById(R.id.tableViewHeading).setVisibility(View.VISIBLE);
                table.setVisibility(View.VISIBLE);
                tableGraphButton.setText("Graph");
                lineChart.setVisibility(View.GONE);
                showTableForDevice();
                Log.d(TAG, "Table now visible");
                break;
            default:
                Log.d(TAG, "Table invisible");
                break;
        }
    }

    private void onClickExport(){
        if(mBMDatabase == null) return;
        // Exporting dialog
        BMAlertDialog dialog = new BMAlertDialog(MainActivity.this,
                "",
                "Exporting \"/sdcard/" + mBMDatabase.getAddress() + ".csv\" ...");
        dialog.show();
        dialog.applyFont(MainActivity.this, "Montserrat-Regular.ttf");
        try {
            Log.d(TAG, "Exporting \"/sdcard/" + mBMDatabase.getDatabaseName() + ".csv\" ...");
            File file = mBMDatabase.setUpExportForStoredData(this, "/sdcard/" + mBMDatabase.getAddress() + ".csv", (int)mBMDatabase.getVersion(), (int)mBMDatabase.getMode());
            //File file = lineChart.export("/sdcard/" + mBMDatabase.getDatabaseName() + ".pdf");

            Uri u1 = Uri.fromFile(file);
            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, mBMDatabase.getAddress() + ".csv");
            sendIntent.putExtra(Intent.EXTRA_STREAM, u1);
            sendIntent.setType("text/html");
            startActivity(sendIntent);

            Log.d(TAG, "Exported \"/sdcard/" + mBMDatabase.getAddress() + ".csv\"");

            dialog.dismiss();
        } catch (IOException e) {
            e.printStackTrace();
        } /*catch (DocumentException e) {
            e.printStackTrace();
        }*/
    }

    private void onClickSettings() {
        //boolean success = scanLeDevice(true);
        //Log.d(TAG, "Start scan result = "+success);
        //return;
        // Connect button pressed, open DeviceListActivity class, with popup windows that scan for devices
        Intent newIntent = new Intent(MainActivity.this, CommandListActivity.class);

        //Create Bundle
        Bundle bundle = new Bundle();
        bundle.putInt("Version", mBMDevice.getVersion());
        newIntent.putExtras(bundle);
        startActivityForResult(newIntent, REQUEST_COMMAND);

    }

    /**
     * Sets up the history pages, e.g. graph, table, etc.
     */
    private void showChartForDevice(){
        // If no database available, stop
        if(mBMDatabase == null) return;
        mBMDatabase.setUpChartForStoredData(lineChart, (int)mBMDatabase.getVersion(), progressIndicatorGraph);
    }

    private void showTableForDevice(){
        // If no database available, stop
        if(mBMDatabase == null) return;
        switch (mBMDatabase.getVersion()) {

            case 23:
                ListView listViewForV22AndV23 = (ListView) findViewById(R.id.table_listView);
                mBMDatabase.setUpTableForStoredData(getApplicationContext(), listViewForV22AndV23, (int)mBMDatabase.getVersion(), mBMDatabase.getMode());
                break;

            case 42:
                ListView listViewFor42 = (ListView) findViewById(R.id.table_listView);
                mBMDatabase.setUpTableForStoredData(getApplicationContext(), listViewFor42, (int)mBMDatabase.getVersion(), mBMDatabase.getMode());
                break;

            case 27:
                ListView listViewFor27 = (ListView) findViewById(R.id.table_listView);
                mBMDatabase.setUpTableForStoredData(getApplicationContext(), listViewFor27, (int)mBMDatabase.getVersion(), mBMDatabase.getMode());
                break;

            case 13:
                ListView listViewFor13 = (ListView) findViewById(R.id.table_listView);
                mBMDatabase.setUpTableForStoredData(getApplicationContext(), listViewFor13, (int)mBMDatabase.getVersion(), mBMDatabase.getMode());
                break;

        }

    }


    private void processCommand() {
        dialogCommand = new BMAlertDialog(MainActivity.this,
                "",
                "Communicating command with " + mBMDatabase.getAddress() + "...");
        dialogCommand.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (commandSuccess) {
                    showAlert("Response from device is " + commandResponse);
                } else {
                    showAlert("Failed to communicate command with " + mBMDatabase.getAddress());
                };
            }
        });
        dialogCommand.show();
        dialogCommand.applyFont(MainActivity.this, "Montserrat-Regular.ttf");
        try {
            Log.d(TAG, "About to send command to device and it is : " + command);
            Utility.sendCommand(mService, command);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void download(){

        if(mBMDevice == null || mBMDatabase == null || mService == null) {
            Log.d(TAG, "In download method and there is no device, database or service");
            dialogStart.dismiss();

            String message =  "Problem downloaded logs from " + mBMDatabase.getAddress();
            showAlert(message);
            return;
        }
        if(!mBMDevice.isDownloadReady()){
            // Device not ready for downloading - display alert message
            dialogStart.dismiss();
            showAlert("Unable to download from device. Please change your current mode.");
            String message =  "Problem downloaded logs from " + mBMDatabase.getAddress();
            showAlert(message);
            return;
        }

        // Downloader
        final BMDownloader downloader = new BMDownloader(mBMDatabase, progressIndicatorDownload);

        //The old code starts below and was immediately under final BMDownloader downloader....


        // Switch out BroadcastReceiver
        LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(UARTStatusChangeReceiver);
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(downloader,
                makeGattUpdateIntentFilter());
        mService.enableTXNotification();

        new Thread(new Runnable() {
            @Override
            public void run() {
                DownloadState state = DownloadState.FAILURE_NOT_DEFINED;
                try {
                    state = mBMDatabase.downloadData(mService, downloader, progressIndicatorDownload);

                } catch (UnsupportedEncodingException e) {
                    Log.d(TAG, "Download method in main and in exception");
                    e.printStackTrace();
                }
                // Switch back in BroadcastReceivers
                LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(downloader);
                LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(UARTStatusChangeReceiver,
                        makeGattUpdateIntentFilter());

                progressIndicatorDownload.dismiss();

                Log.d(TAG, "Download state: " + state);
                String message = (state == DownloadState.SUCCESS)
                        ? "Successfully downloaded logs from " + mBMDatabase.getAddress()
                        : "Failed to download logs from " + mBMDatabase.getAddress();

                //onGattDisconnected();
                showAlert(message);
            }
        }).start();

    }






    private void showAlert(final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final BMAlertDialog dialogEnd = new BMAlertDialog(MainActivity.this,
                        "",
                        message);
                dialogEnd.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {dialogEnd.dismiss();    }
                });
                dialogEnd.show();
                dialogEnd.applyFont(MainActivity.this, "Montserrat-Regular.ttf");
            }
        });
    }
    
    private void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {


        if (mState == UART_PROFILE_CONNECTED) {
            /*
            try {
                Utility.sendCommand(mService, "*qq");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            */
            mService.disconnect();
        }
        else {
            BMAlertDialog dialog = new BMAlertDialog(this,
                    "",
                    "Do you want to quit this Application?");
            dialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            dialog.setNegativeButton("NO", null);

            dialog.show();
            dialog.applyFont(this, "Montserrat-Regular.ttf");
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

    }

    private class CustomArrayAdapter<T> extends ArrayAdapter<T>{
        private final Pattern pattern = Pattern.compile("\\*.*");

        public CustomArrayAdapter(Context context, int resource) {
            super(context, resource);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView textView = (TextView) super.getView(position, convertView, parent);

            CharSequence charSequence = textView.getText();
            if(charSequence == null) return textView;
            int color = getResources().getColor(
                            pattern.matcher(charSequence).matches()
                                ? R.color.bm_command_color
                                : R.color.bm_black);
            textView.setTextColor(color);
            return textView;
        }
    }

    private void dataProcessor(final byte[] txValue)
    {
         int lastPointer =-1;
         int recordsNeeded =-1;
         int globalLogCount =-1;
         int sizePerRecord = 2;
         byte mode =-1;

         int thresholdNumber;
         int[] thresholdTypes;
         short[] thresholds;

          boolean downloading;
          boolean connected;

         int type = 0;
         int index = 0;
         double[][] data = new double[0][0];

        if (txValue.length < 20 && txValue[txValue.length - 1] == ':') {
            lastPointer = convertToInt16(txValue[0], txValue[1]);
            recordsNeeded = convertToInt16(txValue[2], txValue[3]);
            globalLogCount = convertToInt16(txValue[4], txValue[5]);
            sizePerRecord = convertToInt16(txValue[6], txValue[7]);
            mode = txValue[8];

            thresholdNumber = (txValue[9] % 10 != 0 ? 1 : 0)
                    + ((txValue[9] / 10) % 10 != 0 ? 1 : 0);
            thresholdTypes = new int[2];
            thresholdTypes[0] = txValue[9] % 10;
            thresholdTypes[1] = (txValue[9] / 10) % 10;
            thresholds = new short[2];
            thresholds[0] = ByteBuffer.wrap(Arrays.copyOfRange(txValue, 10, 12))
                    .order(ByteOrder.BIG_ENDIAN).getShort();
            thresholds[1] = ByteBuffer.wrap(Arrays.copyOfRange(txValue, 12, 14))
                    .order(ByteOrder.BIG_ENDIAN).getShort();

            Log.d("BMDownloader", "Threshold No.: " + thresholdNumber);
            Log.d("BMDownloader", "Threshold Type: " + thresholdTypes[0] + " | " + thresholdTypes[1]);
            Log.d("BMDownloader", "Threshold: " + thresholds[0] + " | Threshold: " + thresholds[1]);

            Log.d("BMDownloader", "Last Pointer: " + lastPointer +
                    " | Records needed: " + recordsNeeded +
                    " | Global Log Count: " + globalLogCount +
                    " | Size per Record: " + sizePerRecord);

            downloading = true;

            data = new double[10][recordsNeeded];
        } else {
            for (int i = 0; i < txValue.length; i += sizePerRecord) {
                sizePerRecord = 2;
                if (txValue[i] == '.') {
                    downloading = false;
                    mBMDatabase.completedDownload();
                    Log.d("BMDownloader", "Came across terminator . ");
                    return;
                } else if (((txValue[i] == ',') && (txValue[i + 1] == ',')) || (index == recordsNeeded)) {
                    Log.d("BMDownloader", "Came across separator , now changing type.  Type value is : " + type + " and new type value will be : " + (type + 1));
                    i = txValue.length;
                    index = 0;
                    type += 1;
                    continue;
                }
                byte[] record = Arrays.copyOfRange(txValue, i, i + sizePerRecord);
                switch (sizePerRecord) {
                    case 1:
                        // Byte
                        data[type][index] = record[0];
                        index++;
                        break;
                    case 2:
                        // Int (as Short)
                        data[type][index] = ByteBuffer.wrap(record).order(ByteOrder.BIG_ENDIAN).getShort();
                        Log.d("BMDownloader", "Index value is : " + index);
                        index++;
                        break;
                    case 4:
                        // Float
                        data[type][index] = ByteBuffer.wrap(record).order(ByteOrder.BIG_ENDIAN).getFloat();
                        index++;
                        break;
                    default:
                        return;

                }
            }
        }
    }

    protected static final int convertToInt16(byte first, byte second){
        int value = (int) first & 0xFF;
        value *= 256;
        value += (int) second & 0xFF;
        return value;
    }
}
