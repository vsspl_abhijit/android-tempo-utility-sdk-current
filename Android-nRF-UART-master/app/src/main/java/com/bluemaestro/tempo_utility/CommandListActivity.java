package com.bluemaestro.tempo_utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.bluemaestro.tempo_utility.ble.Utility;

import android.text.InputFilter;
import android.text.InputType;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import static com.bluemaestro.tempo_utility.R.id.alert_dialog_radio_group;

/**
 * Created by RichardAtOffice on 23/02/2017.
 */

public class CommandListActivity extends Activity {

    public static final String TAG = "CommandsActivity";
    private String commandToReturn;
    public Button alertDialogConfirmButton, alertDialogCancelButton, radioBtn1,
                    radioBtn2, radioBtn3, radioBtn4;
    public TextView alertDialogTitle, alertDialogDescription, radioButtonDescription;
    public EditText alertDialogEditText;
    public View dialogView;
    public View dialogViewWithRadioButtons;
    public String newInput;
    public AlertDialog optionDialog;
    public AlertDialog optionDialogWithRadioButtons;
    public InputMethodManager imm;
    public View currentFocus;
    private int deviceVersion;

    public enum Selected {
        RADIO_BUTTON_NONE,
        RADIO_BUTTON_ONE,
        RADIO_BUTTON_TWO,
        RADIO_BUTTON_THREE,
        RADIO_BUTTON_FOUR
    };

    public Selected radioButton = Selected.RADIO_BUTTON_NONE;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        int deviceVersion = bundle.getInt("Version");

        View rootView = findViewById(android.R.id.content).getRootView();
        StyleOverride.setDefaultTextColor(rootView, Color.BLACK);
        StyleOverride.setDefaultBackgroundColor(rootView, Color.WHITE);
        StyleOverride.setDefaultFont(rootView, this, "Montserrat-Regular.ttf");



        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
        if (deviceVersion == 42) {
            setContentView(R.layout.commands_list_42);
        } else {
            setContentView(R.layout.commands_list);
        }
        android.view.WindowManager.LayoutParams layoutParams = this.getWindow().getAttributes();
        //layoutParams.gravity = Gravity.TOP;
        //layoutParams.y = 100;

        Button renameButton = (Button) findViewById(R.id.btn_rename);
        renameButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                rename();
            }
        });

        Button loggingIntervalButton = (Button) findViewById(R.id.btn_logging_interval);
        loggingIntervalButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loggingInterval();
            }
        });

        Button sensorIntervalButton = (Button) findViewById(R.id.btn_sensor_interval);
        if (sensorIntervalButton != null) {
            sensorIntervalButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    sensorInterval();
                }
            });
        }

        Button referenceDateButton = (Button) findViewById(R.id.btn_reference_date);
        referenceDateButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                referenceDate();
            }
        });

        Button alarmOneSetButton = (Button) findViewById(R.id.btn_alarm1_set);
        if (alarmOneSetButton != null) {
            alarmOneSetButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    alarmOneSet();
                }
            });
        }

        Button alarmTwoSetButton = (Button) findViewById(R.id.btn_alarm2_set);
        if (alarmTwoSetButton != null) {
            alarmTwoSetButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    alarmTwoSet();
                }
            });
        }

        Button clearAlarmButton = (Button) findViewById(R.id.btn_clear_alarms);
        if (clearAlarmButton != null) {
            clearAlarmButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    clearAlarms();
                }
            });
        }

        Button alarmsOnOffButton = (Button) findViewById(R.id.btn_alarms_on_off);
        if (alarmsOnOffButton != null) {
            alarmsOnOffButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    alarmsOnOrOff();
                }
            });
        }

        Button airplaneModeButton = (Button) findViewById(R.id.btn_airplane_mode);

        if (deviceVersion == 27) {
            airplaneModeButton.setText("Altitude");
        }

        if (airplaneModeButton != null) {
            airplaneModeButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    airplaneModeSet();
                }
            });
        }

        Button transmitPowerButton = (Button) findViewById(R.id.btn_transmit_power);
        if (transmitPowerButton != null ) {
            transmitPowerButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    transmitPower();
                }
            });
        }

        Button clearDataButton = (Button) findViewById(R.id.btn_clear_data);
        if (clearDataButton != null) {
            clearDataButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    clearData();
                }
            });
        }

        Button resetDeviceButton = (Button) findViewById(R.id.btn_reset_device);
        if (resetDeviceButton != null) {
            resetDeviceButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    resetDevice();
                }
            });
        }

        Button changeUnitsbutton = (Button) findViewById(R.id.btn_measurement_units);
        if (changeUnitsbutton != null) {
            changeUnitsbutton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeUnits();
                }
            });
        }

        Button lockDeviceButton = (Button) findViewById(R.id.btn_unlock_device);
        if (lockDeviceButton != null) {
            lockDeviceButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    lockUnlockDevice();
                }
            });
        }

        Button calibrateTemperatureButton = (Button) findViewById(R.id.btn_calibrate_temperature);
        if (calibrateTemperatureButton != null) {
            calibrateTemperatureButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    calibrateTemperature();
                }
            });
        }

        Button clearHumidityButton = (Button) findViewById(R.id.btn_calibrate_humidity);
        if (clearHumidityButton != null) {
            clearHumidityButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    calibrateHumidity();
                }
            });
        }

        Button disableButtonButton = (Button) findViewById(R.id.btn_disable_button);
        if (disableButtonButton != null) {
            disableButtonButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    disableButton();
                }
            });
        }

        Button setDeviceIdButton = (Button) findViewById(R.id.btn_set_device_id);
        if (setDeviceIdButton != null) {
            setDeviceIdButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    setDeviceID();
                }
            });
        }

        Button setAvertFreqButton = (Button) findViewById(R.id.btn_set_advertising_frequency);
        if (setAvertFreqButton != null) {
            setAvertFreqButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    setAdvertFrequency();
                }
            });
        }

        Button firmwareUpgradeButton = (Button) findViewById(R.id.btn_firmware_upgrade);
        if (firmwareUpgradeButton != null) {
            firmwareUpgradeButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    firmwareUpgrade();
                }
            });
        }

        Button blinkDevicebutton = (Button) findViewById(R.id.btn_blink_device);
        if (blinkDevicebutton != null) {
            blinkDevicebutton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    blinkDevice();
                }
            });
        }



        Button commandConsoleButton = (Button) findViewById(R.id.btn_command_console);
        if (commandConsoleButton != null ) {
            commandConsoleButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    commandConsole();
                }
            });
        }

        Button cancelButton = (Button) findViewById(R.id.btn_cancel_command_button);
        if (cancelButton != null) {
            cancelButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancelCommands();
                }
            });
        }

    }

    private  void buildAlertDialog() {

        optionDialog  = new AlertDialog.Builder(this).create();

        LayoutInflater inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.custom_alert_dialog,null);

        optionDialog.setView(dialogView);
        optionDialog.show();
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        alertDialogTitle = (TextView) dialogView.findViewById(R.id.alert_dialog_title);
        alertDialogDescription = (TextView) dialogView.findViewById(R.id.alert_dialog_description);
        alertDialogConfirmButton = (Button) dialogView.findViewById(R.id.alert_dialog_confirm_btn);
        alertDialogCancelButton = (Button) dialogView.findViewById(R.id.alert_dialog_cancel_btn);
        alertDialogEditText = (EditText) dialogView.findViewById(R.id.alert_dialog_textentry);
        alertDialogEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    return true; // Focus will do whatever you put in the logic.
                }
                return false;  // Focus will change according to the actionId

            }
        });

        alertDialogCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                optionDialog.dismiss();
            }
        });

    }

    private void buildAlertDialogRadioButtons() {

        optionDialogWithRadioButtons  = new AlertDialog.Builder(this).create();


        LayoutInflater inflater = getLayoutInflater();
        dialogViewWithRadioButtons = inflater.inflate(R.layout.custom_alert_dialog_radio_buttons,null);

        optionDialogWithRadioButtons.setView(dialogViewWithRadioButtons);
        optionDialogWithRadioButtons.show();
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        alertDialogTitle = (TextView) dialogViewWithRadioButtons.findViewById(R.id.alert_dialog_title);
        alertDialogDescription = (TextView) dialogViewWithRadioButtons.findViewById(R.id.alert_dialog_description);
        alertDialogConfirmButton = (Button) dialogViewWithRadioButtons.findViewById(R.id.alert_dialog_confirm_btn);
        alertDialogCancelButton = (Button) dialogViewWithRadioButtons.findViewById(R.id.alert_dialog_cancel_btn);
        alertDialogEditText = (EditText) dialogViewWithRadioButtons.findViewById(R.id.alert_dialog_textentry);
        radioButtonDescription = (TextView) dialogViewWithRadioButtons.findViewById(R.id.alert_dialog_radio_description);
        radioBtn1 = (RadioButton) dialogViewWithRadioButtons.findViewById(R.id.radio1);
        radioBtn2 = (RadioButton) dialogViewWithRadioButtons.findViewById(R.id.radio2);
        radioBtn3 = (RadioButton) dialogViewWithRadioButtons.findViewById(R.id.radio3);
        radioBtn4 = (RadioButton) dialogViewWithRadioButtons.findViewById(R.id.radio4);
        radioBtn4.setVisibility(View.GONE);

        alertDialogEditText.requestFocus();
        alertDialogEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    return true; // Focus will do whatever you put in the logic.
                }
                return false;  // Focus will change according to the actionId
            }
        });

        alertDialogCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                optionDialogWithRadioButtons.dismiss();
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            }
        });
    }

    private void rename(){
        buildAlertDialog();
        alertDialogTitle.setText("Rename device");
        alertDialogDescription.setText("Enter a new name not exceeding 8 characters");
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                imm.hideSoftInputFromWindow(alertDialogEditText.getWindowToken(), 0);
                newInput = alertDialogEditText.getText().toString();
                returnCommandsToMain("*nam"+newInput);
                optionDialog.dismiss();
            }
        });
    }

    private void loggingInterval(){
        buildAlertDialog();
        alertDialogEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        alertDialogTitle.setText("Set logging interval");
        alertDialogDescription.setText("This is the time interval for logging.  Enter a new interval in seconds beween 2 and 43,200 (12 hours)");
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                imm.hideSoftInputFromWindow(alertDialogEditText.getWindowToken(), 0);
                newInput = alertDialogEditText.getText().toString();
                int newInt = Integer.parseInt(newInput);
                if (newInt < 2) newInt = 2;
                if (newInt > 43200) newInt = 43200;
                newInput = Integer.toString(newInt);
                returnCommandsToMain("*lint"+newInput);
                optionDialog.dismiss();
            }
        });
    }

    private void sensorInterval() {
        buildAlertDialog();
        alertDialogEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        alertDialogTitle.setText("Set sensor interval");
        alertDialogDescription.setText("This is the sensor interval for real time data.  Enter a new interval in seconds beween 2 and 43,200 (12 hours)");
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                imm.hideSoftInputFromWindow(alertDialogEditText.getWindowToken(), 0);
                newInput = alertDialogEditText.getText().toString();
                int newInt = Integer.parseInt(newInput);
                if (newInt < 2) newInt = 2;
                if (newInt > 43200) newInt = 43200;
                newInput = Integer.toString(newInt);
                returnCommandsToMain("*sint"+newInput);
                optionDialog.dismiss();
            }
        });
    }

    private void referenceDate() {
        buildAlertDialog();
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(10);
        alertDialogEditText.setFilters(FilterArray);
        alertDialogEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        alertDialogTitle.setText("Enter reference date");
        alertDialogDescription.setText("This is the date and time for the first log and sets the reference for the time stamps.  This should represent " +
                "when the device is turned on since the first log is recorded on power on.  \n\nEnter the date and time as 10 digits in the following format: yymmddhhmm.  So enter 1702222305 for 2017 February" +
                " 22nd 11:05pm");
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                imm.hideSoftInputFromWindow(alertDialogEditText.getWindowToken(), 0);;
                newInput = alertDialogEditText.getText().toString();
                Utility dateUtility = new Utility();
                String newString = dateUtility.convertNumberIntoDate(newInput);
                returnCommandsToMain("*d"+newInput);
                optionDialog.dismiss();
            }
        });
    }

    private void alarmOneSet() {
        buildAlertDialogRadioButtons();
        alertDialogTitle.setText("Set Temperature Alarm 1");
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(10);
        alertDialogEditText.setFilters(FilterArray);
        alertDialogDescription.setText("Enter a value and then select an alarm parameter.  For example entering 10 and then selecting '<' " +
                "means each time temperature is below 10, this will be logged and an alert will be raised when scanning the device.  " +
                "Enter only whole numbers.");
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        radioBtn4.setVisibility(View.GONE);
        radioBtn3.setVisibility(View.GONE);
        radioBtn2.setText("< (if temperature is below the value you enter, this causes the alarm)");
        radioBtn1.setText("> (if temperature is above the value you entr, this causes the alarm");
        radioButtonDescription.setText("Select from the options below");
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioButton == Selected.RADIO_BUTTON_ONE) {
                    commandToReturn = "*alarm1t<";
                }
                if (radioButton == Selected.RADIO_BUTTON_TWO) {
                    commandToReturn = "*alarm1t>";
                }
                newInput = alertDialogEditText.getText().toString();
                returnCommandsToMain(commandToReturn+newInput);
                optionDialogWithRadioButtons.dismiss();
            }
        });
    }

    private void alarmTwoSet() {
        buildAlertDialogRadioButtons();
        alertDialogTitle.setText("Set Temperature Alarm 2");
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(10);
        alertDialogEditText.setFilters(FilterArray);
        alertDialogDescription.setText("Enter a value and then select an alarm parameter.  For example entering 10 and then selecting '<' " +
                "means each time temperature is below 10, this will be logged and an alert will be raised when scanning the device.  " +
                "Enter only whole numbers.");
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        radioBtn4.setVisibility(View.GONE);
        radioBtn3.setVisibility(View.GONE);
        radioBtn2.setText("< (if temperature is below the value you enter, this causes the alarm)");
        radioBtn1.setText("> (if temperature is above the value you entr, this causes the alarm");
        radioButtonDescription.setText("Select from the options below");
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioButton == Selected.RADIO_BUTTON_ONE) {
                    commandToReturn = "*alarm2t<";
                }
                if (radioButton == Selected.RADIO_BUTTON_TWO) {
                    commandToReturn = "*alarm2t>";
                }
                newInput = alertDialogEditText.getText().toString();
                returnCommandsToMain(commandToReturn+newInput);
                optionDialogWithRadioButtons.dismiss();
            }
        });
    }

    private void clearAlarms() {
        buildAlertDialog();
        alertDialogTitle.setText("Clear alarms");
        alertDialogDescription.setText("This clears the alarms.  Select confirm to continue.");
        alertDialogEditText.setVisibility(View.GONE);
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                returnCommandsToMain("*alrmcl");
                optionDialog.dismiss();
            }
        });
    }

    private void alarmsOnOrOff() {
        buildAlertDialogRadioButtons();
        alertDialogTitle.setText("Turn alarms on or off");
        alertDialogEditText.setVisibility(View.GONE);
        alertDialogDescription.setVisibility(View.GONE);
        radioBtn4.setVisibility(View.GONE);
        radioBtn3.setVisibility(View.GONE);
        radioBtn2.setText("Alarms on");
        radioBtn1.setText("Alarms off");
        radioButtonDescription.setText("Select from the options below");
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                if (radioButton == Selected.RADIO_BUTTON_ONE) {
                    commandToReturn = "*alrm0";
                }
                if (radioButton == Selected.RADIO_BUTTON_TWO) {
                    commandToReturn = "*alrm1";
                }
                returnCommandsToMain(commandToReturn);
                optionDialogWithRadioButtons.dismiss();
            }
        });
    }

    private void airplaneModeSet() {
        buildAlertDialogRadioButtons();
        alertDialogTitle.setText("Turn airplane mode on or off");
        alertDialogEditText.setVisibility(View.GONE);
        alertDialogDescription.setText("This will put the device into airplane mode.  It will advertise for 5 " +
                "minutes after this is set or when the button is pushed.  " +
                "It will continue logging while in airplane mode.");
        radioBtn4.setVisibility(View.GONE);
        radioBtn3.setVisibility(View.GONE);
        radioBtn2.setText("Airplane mode on");
        radioBtn1.setText("Airplane mode off");
        radioButtonDescription.setText("Select from the options below");
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                if (radioButton == Selected.RADIO_BUTTON_ONE) {
                    commandToReturn = "*airoff";
                }
                if (radioButton == Selected.RADIO_BUTTON_TWO) {
                    commandToReturn = "*airon";
                }
                returnCommandsToMain(commandToReturn);
                optionDialogWithRadioButtons.dismiss();
            }
        });
    }

    private void transmitPower() {
        buildAlertDialogRadioButtons();
        alertDialogTitle.setText("Set transmit power");
        alertDialogEditText.setVisibility(View.GONE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        alertDialogDescription.setText("Choose between range and battery power.  -4db increases battery life while reducing range" +
                " while +4db reduces battery life and increases range.");
        radioBtn4.setVisibility(View.GONE);
        radioBtn3.setText("Power -4db");
        radioBtn2.setText("Power +0db");
        radioBtn1.setText("Power +4db (default)");
        radioButtonDescription.setText("Select from the options below");
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                if (radioButton == Selected.RADIO_BUTTON_ONE) {
                    commandToReturn = "*txp0";
                }
                if (radioButton == Selected.RADIO_BUTTON_TWO) {
                    commandToReturn = "*txp1";
                }
                if (radioButton == Selected.RADIO_BUTTON_THREE) {
                    commandToReturn = "*txp2";
                }
                returnCommandsToMain(commandToReturn);
                optionDialogWithRadioButtons.dismiss();
            }
        });
    }

    private void clearData() {
        buildAlertDialog();
        alertDialogTitle.setText("Clear data");
        alertDialogDescription.setText("This clears logged data from the device but leaves settings and name unaffected.  Select confirm to continue.");
        alertDialogEditText.setVisibility(View.GONE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                returnCommandsToMain("*clr");
                optionDialog.dismiss();
            }
        });
    }

    private void resetDevice() {
            buildAlertDialog();
            alertDialogTitle.setText("Reset device");
            alertDialogDescription.setText("This is a factory reset of the device.  All data will be erased and settings will revert to default.");
            alertDialogEditText.setVisibility(View.GONE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    returnCommandsToMain("*rboot");
                    optionDialog.dismiss();
                }
            });
    }

    private void changeUnits() {
        buildAlertDialogRadioButtons();
        alertDialogTitle.setText("Set units of measure");
        alertDialogEditText.setVisibility(View.GONE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        alertDialogDescription.setVisibility(View.GONE);
        radioBtn4.setVisibility(View.GONE);
        radioBtn3.setVisibility(View.GONE);
        radioBtn2.setText("Celsius");
        radioBtn1.setText("Fahrenheit");
        radioButtonDescription.setText("Select from the options below");
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioButton == Selected.RADIO_BUTTON_ONE) {
                    commandToReturn = "*unitsf";
                }
                if (radioButton == Selected.RADIO_BUTTON_TWO) {
                    commandToReturn = "*unitsc";
                }
                returnCommandsToMain(commandToReturn);
                optionDialogWithRadioButtons.dismiss();
            }
        });
    }

    private void lockUnlockDevice() {
            buildAlertDialog();
            alertDialogEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
            alertDialogTitle.setText("Lock and unlock the device");
            alertDialogDescription.setText("Enter a pin to prevent settings in the device being changed.  The pin should be 4 numbers.  If a pin is set, the pin will" +
                    " need to be rentered before settings can be changed or data downloaded.  If you forget the pin, removing the battery will reset it.");
            alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialogEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                    imm.hideSoftInputFromWindow(alertDialogEditText.getWindowToken(), 0);
                    newInput = alertDialogEditText.getText().toString();
                    returnCommandsToMain("*pwd"+newInput);
                    optionDialog.dismiss();
                }
            });
    }

    private void calibrateHumidity() {
            buildAlertDialog();
            alertDialogEditText.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED);
            alertDialogTitle.setText("Calibrate humidity");
            alertDialogDescription.setText("Enter an offset (positive or negative) to calibrate humidity.  This should be in whole numbers rounded to the nearest whole %RH");
            alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    newInput = alertDialogEditText.getText().toString();
                    returnCommandsToMain("*ch"+newInput);
                    optionDialog.dismiss();
            }
        });
    }

    private void calibrateTemperature() {
        buildAlertDialog();
        alertDialogEditText.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED);
        alertDialogTitle.setText("Calibrate temperature");
        alertDialogDescription.setText("Enter an offset (positive or negative) to calibrate temperature.  This should be in the units of measurement" +
                " set for the device and rounded to the nearest whole º");
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                newInput = alertDialogEditText.getText().toString();
                returnCommandsToMain("*ct"+newInput);
                optionDialog.dismiss();
            }
        });
    }

    private void disableButton() {
        buildAlertDialogRadioButtons();
        alertDialogTitle.setText("Diable button");
        alertDialogEditText.setVisibility(View.GONE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        alertDialogDescription.setVisibility(View.GONE);
        radioBtn4.setVisibility(View.GONE);
        radioBtn3.setVisibility(View.GONE);
        radioBtn2.setVisibility(View.GONE);
        radioBtn1.setText("Toggle button disable");
        radioButtonDescription.setText("This will disable the button from turning the device off or on.  A button push will still" +
                " cause the device to advertise (for example if it is in airplane mode).  This command is a toggle, so if the button is disabled, selecting" +
        " this will cause the button to be enabled and vice versa");
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioButton == Selected.RADIO_BUTTON_ONE) {
                    commandToReturn = "*bd";
                }
                returnCommandsToMain(commandToReturn);
                optionDialogWithRadioButtons.dismiss();
            }
        });
    }

    private void setDeviceID() {
        buildAlertDialog();
        alertDialogEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        alertDialogTitle.setText("Set device ID");
        alertDialogDescription.setText("This provides an additional identifier for a device in addition to its name.  This is useful if several devices" +
                " could have similar names or you would like to group devices with a common identifier.  Enter a number between 0 and 255");
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                newInput = alertDialogEditText.getText().toString();
                returnCommandsToMain("*id"+newInput);
                optionDialog.dismiss();
            }
        });
    }

    private void setAdvertFrequency() {
        buildAlertDialogRadioButtons();
        alertDialogTitle.setText("Set advertising frequency");
        alertDialogEditText.setVisibility(View.GONE);
        alertDialogDescription.setText("Setting a lower frequency of advertising will extend battery life but may cause a slight delay in the " +
                "device being detected in a scan.");
        radioBtn4.setText("10000 ms (low frequency)");
        radioBtn3.setText("800 ms");
        radioBtn2.setText("600 ms");
        radioBtn1.setText("300 ms (high frequency)");
        radioButtonDescription.setText("Select from the options below");
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioButton == Selected.RADIO_BUTTON_ONE) {
                    commandToReturn = "*sadv1000";
                }
                if (radioButton == Selected.RADIO_BUTTON_TWO) {
                    commandToReturn = "*sadv800";
                }
                if (radioButton == Selected.RADIO_BUTTON_THREE) {
                    commandToReturn = "*sadv600";
                }
                if (radioButton == Selected.RADIO_BUTTON_FOUR) {
                    commandToReturn = "*sadv300";
                }
                returnCommandsToMain(commandToReturn);
                optionDialogWithRadioButtons.dismiss();
            }
        });
    }

    private void firmwareUpgrade() {
        buildAlertDialog();
        alertDialogTitle.setText("Firmware upgrade");
        alertDialogDescription.setText("This causes the device to enter bootloader mode ready to receive" +
                " new firmware.  Note once the device enters this mode, it will advertise itself as DFUTarg and" +
                " will not be visible in this app until it exits bootloader mode.");
        alertDialogEditText.setVisibility(View.GONE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                returnCommandsToMain("*dfu");
                optionDialog.dismiss();
            }
        });

    }

    private void blinkDevice() {
        buildAlertDialog();
        alertDialogTitle.setText("Blink device LED");
        alertDialogDescription.setText("This will cause the device LED to blink if it has this feature included in its firmware");
        alertDialogEditText.setVisibility(View.GONE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                returnCommandsToMain("*blink");
                optionDialog.dismiss();
            }
        });


    }

    private void commandConsole() {
        buildAlertDialog();
        alertDialogTitle.setText("Command console");
        alertDialogDescription.setText("This provides access to a command console where commands" +
                " can be typed directly into the device.  Please refer to the associated API document" +
                " for your device for a list of commands.");
        alertDialogEditText.setVisibility(View.GONE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        alertDialogConfirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                returnCommandsToMain("console");
                optionDialog.dismiss();
            }
        });
    }

    private void cancelCommands() {
        finish();
    }

    private void returnCommandsToMain(String command){

        Bundle b = new Bundle();
        b.putString("Command", command);

        Intent result = new Intent();
        result.putExtras(b);
        setResult(Activity.RESULT_OK, result);
        finish();

    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio1:
                if (checked)
                    radioButton = Selected.RADIO_BUTTON_ONE;
                    break;
            case R.id.radio2:
                if (checked)
                    radioButton = Selected.RADIO_BUTTON_TWO;
                    break;
            case R.id.radio3:
                if (checked)
                    radioButton = Selected.RADIO_BUTTON_THREE;
                    break;
            case R.id.radio4:
                if (checked)
                    radioButton = Selected.RADIO_BUTTON_FOUR;
                    break;



        }
    }




}
