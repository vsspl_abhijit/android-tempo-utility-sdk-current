package com.bluemaestro.tempo_utility.views.tables;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TableRow;

import com.bluemaestro.tempo_utility.R;
import com.bluemaestro.tempo_utility.views.generic.BMTextView;

import java.util.List;

/**
 * Created by Garrett on 06/09/2016.
 */
public class TableAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private List<List<String>> table;

    private LinearLayout.LayoutParams params;

    public TableAdapter(Context context, List<List<String>> table){
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.table = table;

        this.params = new LinearLayout.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT,
                1f);
    }

    @Override
    public int getCount() {
        return table.size();
    }

    @Override
    public Object getItem(int position) {
        return table.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        List<String> row = table.get(position);

        ViewGroup vg;
        if(convertView != null){
            vg = (ViewGroup) convertView;
        } else{
            vg = (ViewGroup) inflater.inflate(R.layout.table_element, null);
            vg.setOnClickListener(null);
            int width = vg.getWidth() / row.size();
            for(int i = 0; i < row.size(); i++){
                BMTextView view = new BMTextView(context);
                view.setGravity(Gravity.CENTER);
                // Set "time" values to have smaller font
                view.setTextSize(10);
                view.setPadding(0, 0, 0, 0);
                view.setLayoutParams(params);
                view.setWidth(width);
                vg.addView(view);
            }
        }

        for(int i = 0; i < row.size(); i++){
            String element = row.get(i);
            BMTextView view = (BMTextView) vg.getChildAt(i);
            view.setText(element);
        }
        return vg;
    }
}
