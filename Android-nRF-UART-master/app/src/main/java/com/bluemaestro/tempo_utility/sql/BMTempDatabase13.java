package com.bluemaestro.tempo_utility.sql;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Color;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.bluemaestro.tempo_utility.UartService;
import com.bluemaestro.tempo_utility.devices.BMTemp13;
import com.bluemaestro.tempo_utility.devices.BMTempHumi23;
import com.bluemaestro.tempo_utility.sql.downloading.BMDownloader;
import com.bluemaestro.tempo_utility.sql.downloading.DownloadState;
import com.bluemaestro.tempo_utility.views.dialogs.BMProgressIndicator;
import com.bluemaestro.tempo_utility.views.graphs.BMLineChart;
import com.github.mikephil.charting.charts.Chart;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by Garrett on 23/08/2016.
 */
public class BMTempDatabase13 extends BMDatabase {


    // Keys
    private static final String KEY_TEMP = "temperature";
    private static final String KEY_TIME = "time";
    private static final String LAST_DOWNLOAD_DATE = "last_download_date";

    private BMTemp13 mBMTemp13;

    public BMTempDatabase13(Context context, BMTemp13 bmTempHumi) {
        super(context, bmTempHumi);
        CREATE_TABLE_DATA += "," + KEY_TEMP + " REAL";
        CREATE_TABLE_DATA += "," + KEY_TIME + " TIMESTAMP";
        this.mBMTemp13 = bmTempHumi;

    }

    public static class Readings {
        int indexValue;
        float temperatureValue;
        String dateStamp;

        public int getIndexValue() {
            return indexValue;
        }

        public void setIndexValue(int value) {
            this.indexValue = value;
        }

        public float getTemparatureValue() {
            return temperatureValue;
        }

        public void setTemperatureValue(float value) {
            this.temperatureValue = value;
        }

        public String getDateStamp() {
            return dateStamp;
        }

        public void setDateStamp(String value) {
            this.dateStamp = value;
        }


    }


    @Override
    public ContentValues addToValues(ContentValues values, String key, double data) {
        if(key.equals(KEY_TEMP)){
            values.put(key, data / 10.0);
        }
        return values;
    }

    @Override
    public DownloadState downloadData(UartService service, BMDownloader downloader, BMProgressIndicator progress) throws UnsupportedEncodingException {
            return downloadData(service, downloader,
                    new String[]{
                            KEY_TEMP
                },
                true, mBMTemp13.getReferenceDate(), progress);
    }

    @Override
    public void displayAsInfo(ArrayAdapter<String> listAdapter){
        super.displayAsInfo(listAdapter);
    }

    @Override
    public void displayAsChart(Chart chart, BMProgressIndicator progress) {
        if (!(chart instanceof BMLineChart)) return;
        BMLineChart lineChart = (BMLineChart) chart;
        displayAsChart(
                getVersion(),
                lineChart,
                new String[]{
                        KEY_TEMP
                },
                new String[]{
                        KEY_TEMP
                },
                new int[]{
                        Color.RED
                },
                progress
        );
    }

    @Override
    public void displayAsTable(Context context, ListView listView) {
        displayAsTable(context, listView, (int)mBMTemp13.getVersion(), mBMTemp13.getTempUnits());
    }

    @Override
    public File export(Context context, String filename) throws IOException {
        return export(context, filename, (int)mBMTemp13.getVersion(), mBMTemp13.getTempUnits());
    }
}
