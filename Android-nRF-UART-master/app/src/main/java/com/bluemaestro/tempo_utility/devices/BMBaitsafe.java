package com.bluemaestro.tempo_utility.devices;

import android.bluetooth.BluetoothDevice;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bluemaestro.tempo_utility.Log;
import com.bluemaestro.tempo_utility.R;
import com.bluemaestro.tempo_utility.ble.Utility;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.data.Entry;
import com.bluemaestro.tempo_utility.views.graphs.BMLineChart;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Garrett on 15/08/2016.
 */
public class BMBaitsafe extends BMDevice {

    private static final Pattern burst_pattern =
            Pattern.compile("x(-?[0-9]+)y(-?[0-9]+)z(-?[0-9]+)");

    // Battery level
    private int battery;

    private int loggingInterval;
    private int globalLogCount;

    private int thresholdOneMovementCount;
    private int thresholdTwoMovementCount;

    private int thresholdOneLastDetect;
    private int thresholdOneLastDetectRate;

    private int thresholdTwoLastDetect;
    private int thresholdTwoLastDetectRate;

    private int sensitivityThresholdOne;
    private int sensitivityThresholdTwo;

    private int forceLevel;

    private int movementTimeInterval;

    private int referenceDateRawNumber;

    private int buttonPressCount;



    public BMBaitsafe(BluetoothDevice device, byte id) {
        super(device, id);
    }

    public int getBatteryLevel() {
        return battery;
    }

    public int getLoggingInterval() {
        return loggingInterval;
    }
    public int getGlobalLogCount() {
        return globalLogCount;
    }

    public int getThresholdOneMovementCount() {
        return thresholdOneMovementCount;
    }
    public int getThresholdOneLastDetect() { return thresholdOneLastDetect;}
    public int getThresholdOneLastDetectRate() { return thresholdOneLastDetectRate; }

    public int getThresholdTwoMovementCount() {
        return thresholdTwoMovementCount;
    }
    public int getThresholdTwoLastDetect() { return thresholdTwoLastDetect;}
    public int getThresholdTwoLastDetectRate() { return thresholdTwoLastDetectRate; }


    public int getSensitivityThresholdOne() {
        return sensitivityThresholdOne;
    }
    public int getSensitivityThresholdTwo() {
        return sensitivityThresholdTwo;
    }

    public int getForceLevel() { return forceLevel; }

    public int getMovementTimeInterval() {
        return movementTimeInterval;
    }

    public String getReferenceDate() {
        Utility dateUtility = new Utility();
        return dateUtility.convertNumberIntoDate(String.valueOf(referenceDateRawNumber));
    }

    public int getButtonPressCount() {
        return buttonPressCount;
    }

    @Override
    public void updateWithData(int rssi, String name, byte[] mData, byte[] sData){
        super.updateWithData(rssi, name, mData, sData);

        this.battery = mData[4];

        this.loggingInterval = convertToInt16(mData[5], mData[6]);
        this.globalLogCount = convertToInt16(mData[7], mData[8]);

        this.thresholdOneMovementCount = convertToInt16(mData[9], mData[10]);
        this.thresholdTwoMovementCount = convertToInt16(mData[11], mData[12]);

        this.sensitivityThresholdOne = convertToInt16(sData[3], sData[4]);
        this.sensitivityThresholdTwo = convertToInt16(sData[5], sData[6]);

        this.forceLevel = convertToUInt8(sData[7]);

        this.movementTimeInterval = convertToInt16(sData[8], sData[9]);

        this.sensitivityThresholdOne = convertToUInt8(sData[10]);
        this.sensitivityThresholdTwo = convertToUInt8(sData[11]);

        this.referenceDateRawNumber = ((0xFF & sData[12]) << 24) | ((0xFF & sData[13]) << 16) |  ((0xFF & sData[14]) << 8) | (0xFF & sData[15]);
        Log.d("BMTempHumi", "referenceDateNumber" + this.referenceDateRawNumber);

        this.buttonPressCount = (convertToUInt8(sData[16]));
    }

    @Override
    public void updateViewGroup(ViewGroup vg){
        super.updateViewGroup(vg);
        final TextView tvbatt = (TextView) vg.findViewById(R.id.battery);

        final TextView tvLoggingInterval = (TextView) vg.findViewById(R.id.logging_interval_v32);
        final TextView tvGlobalLogCount = (TextView) vg.findViewById(R.id.global_log_count_v32);

        final TextView thresholdOneLabelLineOne = (TextView) vg.findViewById(R.id.threshold_one_label_LineOne);
        final TextView thresholdOneLabelLineTwo = (TextView) vg.findViewById(R.id.threshold_one_label_LineTwo);
        final TextView movementThresholdOne = (TextView) vg.findViewById(R.id.threshold_one_value);
        final TextView sensitivityLevelOne = (TextView) vg.findViewById(R.id.sensitivityThresholdOne);

        final TextView thresholdTwoLabelLineOne = (TextView) vg.findViewById(R.id.threshold_two_label_LineOne);
        final TextView thresholdTwoLabelLineTwo = (TextView) vg.findViewById(R.id.threshold_two_label_LineTwo);
        final TextView movementThresholdTwo = (TextView) vg.findViewById(R.id.threshold_two_value);
        final TextView sensitivityLevelTwo = (TextView) vg.findViewById(R.id.sensitivityThresholdTwo);

        final TextView buttonPressCount = (TextView) vg.findViewById(R.id.buttonPressCount);

        final TextView referenceDate = (TextView) vg.findViewById(R.id.reference_date_for_v32);


        // Set visible
        vg.findViewById(R.id.threshold_one_box).setVisibility(View.VISIBLE);
        vg.findViewById(R.id.threshold_two_box).setVisibility(View.VISIBLE);
        vg.findViewById(R.id.logging_interval_v32).setVisibility(View.VISIBLE);
        vg.findViewById(R.id.global_log_count_v32).setVisibility(View.VISIBLE);
        vg.findViewById(R.id.threshold_one_value).setVisibility(View.VISIBLE);
        vg.findViewById(R.id.threshold_two_value).setVisibility(View.VISIBLE);
        vg.findViewById(R.id.buttonPressCount).setVisibility(View.VISIBLE);
        vg.findViewById(R.id.reference_date_for_v32).setVisibility(View.VISIBLE);

        // Battery
        int battery = getBatteryLevel();
        if(battery >= 0 && battery <= 100){
            tvbatt.setVisibility(View.VISIBLE);
            tvbatt.setText("Battery: " + battery + "%");
        }

        // Logging interval and elapsed
        tvLoggingInterval.setVisibility(View.VISIBLE);
        tvLoggingInterval.setText("Logging Interval: " + getLoggingInterval() + " s");
        tvGlobalLogCount.setVisibility(View.VISIBLE);
        tvGlobalLogCount.setText("Interval Count: " + getGlobalLogCount());


        // Threshold One
        thresholdOneLabelLineOne.setVisibility(View.VISIBLE);
        thresholdOneLabelLineOne.setText("CHANNEL");
        thresholdOneLabelLineTwo.setVisibility(View.VISIBLE);
        thresholdOneLabelLineTwo.setText("ONE");
        movementThresholdOne.setVisibility(View.VISIBLE);
        movementThresholdOne.setText("" + getThresholdOneMovementCount());

        // Threshold Two
        thresholdTwoLabelLineOne.setVisibility(View.VISIBLE);
        thresholdTwoLabelLineOne.setText("CHANNEL");
        thresholdTwoLabelLineTwo.setVisibility(View.VISIBLE);
        thresholdTwoLabelLineTwo.setText("TWO");
        movementThresholdTwo.setVisibility(View.VISIBLE);
        movementThresholdTwo.setText("" + getThresholdTwoMovementCount());

        // Button press count
        buttonPressCount.setVisibility(View.VISIBLE);
        buttonPressCount.setText("Button Press Count: " + getButtonPressCount());

    }

    public void updateViewGroupForDetails(ViewGroup vg){

        //Device Information
        TextView name = (TextView) vg.findViewById(R.id.deviceName);
        TextView address = (TextView) vg.findViewById(R.id.deviceAddress);
        TextView battery = (TextView) vg.findViewById(R.id.batteryLevel);
        TextView signal = (TextView) vg.findViewById(R.id.rssiStrength);
        TextView referenceDate = (TextView) vg.findViewById(R.id.referenceDate);
        TextView lastDownloadDate = (TextView) vg.findViewById(R.id.lastDownloadDate);
        TextView version = (TextView) vg.findViewById(R.id.versionNumber);

        //Current Values
        TextView unitsForTemp = (TextView) vg.findViewById(R.id.unitsForTempDeviceInfo);
        TextView temperatureValue = (TextView) vg.findViewById(R.id.tempValue);
        TextView humidityValue = (TextView) vg.findViewById(R.id.humValue);
        TextView dewpointValue = (TextView) vg.findViewById(R.id.dewpValue);

        //Last 24 Hour Values
        TextView high24Temp = (TextView) vg.findViewById(R.id.high24TempValue);
        TextView avg24Temp = (TextView) vg.findViewById(R.id.avg24TempValue);
        TextView low24Temp = (TextView) vg.findViewById(R.id.low24TempValue);
        TextView high24Humi = (TextView) vg.findViewById(R.id.high24HumValue);
        TextView avg24Humi = (TextView) vg.findViewById(R.id.avg24HumValue);
        TextView low24Humi = (TextView) vg.findViewById(R.id.low24HumValue);
        TextView high24Dewp = (TextView) vg.findViewById(R.id.high24DewpValue);
        TextView avg24Dewp = (TextView) vg.findViewById(R.id.avg24DewpValue);
        TextView low24Dewp = (TextView) vg.findViewById(R.id.low24DewpValue);

        //Last Forever values
        TextView highestTemp = (TextView) vg.findViewById(R.id.highestTempValue);
        TextView lowestTemp = (TextView) vg.findViewById(R.id.lowestTempValue);
        TextView highestHumi = (TextView) vg.findViewById(R.id.highestHumValue);
        TextView lowestHumi = (TextView) vg.findViewById(R.id.lowestHumValue);

        //Set Values
        name.setText(getName());
        address.setText("Device ID: " + getAddress());
        battery.setText("Battery: " + getBatteryLevel() + "%");
        signal.setText("Signal: " + getRSSI() + "dB");
        version.setText("Version: " + getVersion());



        referenceDate.setText("Reference Date : " + getReferenceDate());
        lastDownloadDate.setVisibility(View.GONE);

    }

    @Override
    public void setupChart(Chart chart, String command){
        if(chart instanceof BMLineChart && command.equals("*bur")){
            // If we sent the "*bur" command, setup the chart
            BMLineChart lineChart = (BMLineChart) chart;
            lineChart.init("", 5);
            lineChart.setLabels(
                    new String[]{"X", "Y", "Z"},
                    new int[]{Color.RED, Color.BLUE, Color.BLACK}
            );
        }
    }


    @Override
    public void updateChart(Chart chart, String text){
        if(chart instanceof BMLineChart){
            BMLineChart lineChart = (BMLineChart) chart;
            // Only continue if it's XYZ values
            Matcher matcher = burst_pattern.matcher(text);
            if(!matcher.matches()) return;

            // Parse values
            int[] value = new int[3];
            for(int i = 0; i < value.length; i++) {
                value[i] = new Integer(matcher.group(i + 1).trim());
            }
            // Insert values as new entries
            int index = lineChart.getEntryCount() / 3;
            lineChart.addEntry("X", new Entry(index, value[0]));
            lineChart.addEntry("Y", new Entry(index, value[1]));
            lineChart.addEntry("Z", new Entry(index, value[2]));
        }
    }
}
