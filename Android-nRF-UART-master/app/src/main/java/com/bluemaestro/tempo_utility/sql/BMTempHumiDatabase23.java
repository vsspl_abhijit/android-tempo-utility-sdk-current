package com.bluemaestro.tempo_utility.sql;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Color;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.bluemaestro.tempo_utility.UartService;
import com.bluemaestro.tempo_utility.devices.BMTempHumi23;
import com.bluemaestro.tempo_utility.sql.downloading.BMDownloader;
import com.bluemaestro.tempo_utility.sql.downloading.DownloadState;
import com.bluemaestro.tempo_utility.views.dialogs.BMProgressIndicator;
import com.bluemaestro.tempo_utility.views.graphs.BMLineChart;
import com.github.mikephil.charting.charts.Chart;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by Garrett on 23/08/2016.
 */
public class BMTempHumiDatabase23 extends BMDatabase {


    // Keys
    private static final String KEY_TEMP = "temperature";
    private static final String KEY_HUMI = "humidity";
    private static final String KEY_DEWP = "dew_point";
    private static final String KEY_TIME = "time";
    private static final String LAST_DOWNLOAD_DATE = "last_download_date";

    private BMTempHumi23 mBMTempHumi23;

    public BMTempHumiDatabase23(Context context, BMTempHumi23 bmTempHumi) {
        super(context, bmTempHumi);
        CREATE_TABLE_DATA += "," + KEY_TEMP + " REAL";
        CREATE_TABLE_DATA += "," + KEY_HUMI + " REAL";
        CREATE_TABLE_DATA += "," + KEY_DEWP + " REAL";
        CREATE_TABLE_DATA += "," + KEY_TIME + " TIMESTAMP";
        this.mBMTempHumi23 = bmTempHumi;

    }

    public static class Readings {
        int indexValue;
        float temperatureValue;
        float humidityValue;
        float dewpointValue;
        String dateStamp;

        public int getIndexValue() {
            return indexValue;
        }

        public void setIndexValue(int value) {
            this.indexValue = value;
        }

        public float getTemparatureValue() {
            return temperatureValue;
        }

        public void setTemperatureValue(float value) {
            this.temperatureValue = value;
        }

        public float getHumidityValue() {
            return humidityValue;
        }

        public void setHumidityValue(float value) {
            this.humidityValue = value;
        }

        public float getDewpointValue() {
            return dewpointValue;
        }

        public void setDewpointValue(float value) {
            this.dewpointValue = value;
        }

        public String getDateStamp() {
            return dateStamp;
        }

        public void setDateStamp(String value) {
            this.dateStamp = value;
        }


    }


    @Override
    public ContentValues addToValues(ContentValues values, String key, double data) {
        if(key.equals(KEY_TEMP) || key.equals(KEY_DEWP) || key.equals(KEY_HUMI)){
            values.put(key, data / 10.0);
        }
        return values;
    }

    @Override
    public DownloadState downloadData(UartService service, BMDownloader downloader, BMProgressIndicator progress) throws UnsupportedEncodingException {
        return downloadData(service, downloader,
                new String[]{
                        KEY_TEMP,
                        KEY_HUMI,
                        KEY_DEWP
                },
                true, mBMTempHumi23.getReferenceDate(), progress);
    }

    @Override
    public void displayAsInfo(ArrayAdapter<String> listAdapter){
        super.displayAsInfo(listAdapter);
    }

    @Override
    public void displayAsChart(Chart chart, BMProgressIndicator progress) {
        if (!(chart instanceof BMLineChart)) return;
        BMLineChart lineChart = (BMLineChart) chart;
        displayAsChart(
                getVersion(),
                lineChart,
                new String[]{
                        KEY_TEMP,
                        KEY_HUMI,
                        KEY_DEWP
                },
                new String[]{
                        KEY_TEMP,
                        KEY_DEWP
                },
                new int[]{
                        Color.RED,
                        Color.BLUE,
                        Color.GREEN
                },
                progress
        );
    }

    @Override
    public void displayAsTable(Context context, ListView listView) {
        displayAsTable(context, listView, (int)mBMTempHumi23.getVersion(), mBMTempHumi23.getTempUnits());
    }

    @Override
    public File export(Context context, String filename) throws IOException {
        return export(context, filename, (int)mBMTempHumi23.getVersion(), mBMTempHumi23.getTempUnits());
    }
}
