package com.bluemaestro.tempo_utility.views.generic;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by Garrett on 05/08/2016.
 */
public class BMRelativeLayout extends RelativeLayout {

    public BMRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
