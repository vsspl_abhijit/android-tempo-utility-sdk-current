package com.bluemaestro.tempo_utility.devices;

import android.bluetooth.BluetoothDevice;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bluemaestro.tempo_utility.Log;
import com.bluemaestro.tempo_utility.R;
import com.bluemaestro.tempo_utility.ble.Utility;
import com.bluemaestro.tempo_utility.sql.BMDatabase;
import com.bluemaestro.tempo_utility.views.graphs.BMLineChart;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.data.Entry;

import org.w3c.dom.Text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Garrett on 17/08/2016.
 */
public class BMTemp13 extends BMDevice {

    private static final Pattern burst_pattern =
            Pattern.compile("T(-?[0-9]+\\.[0-9])");

    // Battery level
    private int battery;

    // Reference Date
    private int referenceDateRawNumber;

    // Logging interval
    private int loggingInterval;

    // Global log count
    private int globalLogCount;

    // Current temperature, humidity, and dew point
    private double currTemperature;
    private double currHumidity;
    private double currDewPoint;

    // Number of threshold breaches
    private int numBreach;

    // Highest temperature and humidity recorded
    private double highTemperature;
    private double highHumidity;

    // Lowest temperature and humidity recorded
    private double lowTemperature;
    private double lowHumidity;

    // Highest temperature, humidity, and dew point recorded in last 24 hours
    private double high24Temperature;
    private double high24Humidity;
    private double high24DewPoint;

    // Lowest temperature, humidity, and dew point recorded in last 24 hours
    private double low24Temperature;
    private double low24Humidity;
    private double low24DewPoint;

    // Average temperature, humidity, and dew point recorded in last 24 hours
    private double avg24Temperature;
    private double avg24Humidity;
    private double avg24DewPoint;

    public static final String UNITSF = "º F";
    public static final String UNITSC = "º C";


    //Class ID
    private int classIDNumber;

    public BMTemp13(BluetoothDevice device, byte id) {
        super(device, id);
    }

    @Override
    public void updateWithData(int rssi, String name, byte[] mData, byte[] sData){


        super.updateWithData(rssi, name, mData, sData);
        this.battery = mData[4];

        this.loggingInterval = convertToUInt16(mData[5], mData[6]);
        BMDatabase database = BMDeviceMap.INSTANCE.getBMDatabase(getAddress());
        database.setLoggingInterval(loggingInterval);
        this.globalLogCount = convertToUInt16(mData[7], mData[8]);

        this.currTemperature = convertToInt16(mData[9], mData[10]) / 10.0;
        this.currHumidity = convertToInt16(mData[11], mData[12]) / 10.0;
        this.currDewPoint = convertToInt16(mData[13], mData[14]) / 10.0;

        this.mode = mData[15];

        this.numBreach = convertToInt8(mData[16]);

        this.highTemperature = convertToInt16(sData[3], sData[4]) / 10.0;
        this.highHumidity = convertToInt16(sData[5], sData[6]) / 10.0;

        this.lowTemperature = convertToInt16(sData[7], sData[8]) / 10.0;
        this.lowHumidity = convertToInt16(sData[9], sData[10]) / 10.0;

        this.high24Temperature = convertToInt16(sData[11], sData[12]) / 10.0;
        this.high24Humidity = convertToInt16(sData[13], sData[14]) / 10.0;
        this.high24DewPoint = (this.high24Temperature  - ((100 - this.high24Humidity) /5));

        this.low24Temperature = convertToInt16(sData[15], sData[16]) / 10.0;
        this.low24Humidity = convertToInt16(sData[17], sData[18]) / 10.0;
        this.low24DewPoint = (this.low24Temperature  - ((100 - this.low24Humidity) /5));

        this.avg24Temperature = convertToInt16(sData[19], sData[20]) / 10.0;
        this.avg24Humidity = convertToInt16(sData[21], sData[22]) / 10.0;
        this.avg24DewPoint = (this.avg24Temperature  - ((100 - this.avg24Humidity) /5));

        this.classIDNumber = convertToUInt8(sData[23]);

        this.referenceDateRawNumber = ((0xFF & sData[24]) << 24) | ((0xFF & sData[25]) << 16) |  ((0xFF & sData[26]) << 8) | (0xFF & sData[27]);
        Log.d("BMTempHumi", "referenceDateNumber" + this.referenceDateRawNumber);
    }

    public int getBatteryLevel(){
        return battery;
    }

    public int getLoggingInterval(){
        return loggingInterval;
    }

    public int getGlobalLogCount() { return globalLogCount; }

    public double getCurrentTemperature(String units){
        if (units.matches(UNITSF)) return ((currTemperature * 1.8) + 32);
        return  currTemperature;
    }
    public double getCurrentHumidity(){
        return currHumidity;
    }
    public double getCurrentDewPoint(String units){
        if (units.matches(UNITSF)) return ((currDewPoint * 1.8) + 32);
        return currDewPoint;
    }
    public boolean isInAeroplaneMode(){
        return mode % 100 == 5;
    }
    public boolean isInFahrenheit(){
        return mode >= 100;
    }
    public String getTempUnits(){
        return isInFahrenheit() ? UNITSF : UNITSC;
    }

    public int getNumBreach(){
        return numBreach;
    }

    public double getHighestTemperature(String units){
        if (units.matches(UNITSF)) return ((highTemperature * 1.8) + 32);
        return highTemperature;
    }
    public double getHighestHumidity(){
        return highHumidity;
    }

    public double getLowestTemperature(String units){
        if (units.matches(UNITSF)) return ((low24Temperature * 1.8) + 32);
        return lowTemperature;
    }
    public double getLowestHumidity() {
        return lowHumidity;
    }

    public double getHighest24Temperature(String units){
        if (units.matches(UNITSF)) return ((high24Temperature * 1.8) + 32);
        return high24Temperature;
    }
    public double getHighest24Humidity(){
        return high24Humidity;
    }
    public double getHighest24DewPoint(String units){
        if (units.matches(UNITSF)) return ((high24DewPoint * 1.8) + 32);
        return high24DewPoint;
    }

    public double getLowest24Temperature(String units){
        if (units.matches(UNITSF)) return ((low24Temperature * 1.8) + 32);
        return low24Temperature;
    }
    public double getLowest24Humidity(){
        return low24Humidity;
    }
    public double getLowest24DewPoint(String units){
        if (units.matches(UNITSF)) return ((low24DewPoint * 1.8) + 32);
        return low24DewPoint;
    }

    public double getAverage24Temperature(String units){
        if (units.matches(UNITSF)) return ((avg24Temperature * 1.8) + 32);
        return avg24Temperature;
    }
    public double getAverage24Humidity(){
        return avg24Humidity;
    }
    public double getAverage24DewPoint(String units){
        if (units.matches(UNITSF)) return ((avg24DewPoint * 1.8) + 32);
        return avg24DewPoint;
    }
    public String getReferenceDate() {
        Utility dateUtility = new Utility();
        return dateUtility.convertNumberIntoDate(String.valueOf(referenceDateRawNumber));
    }
    public String getLockedUnlocked() {
        if (((this.mode % 100)/10) > 1) return "Device Locked";
        return "Device Unlocked";
    }
    public String classID() {
        return "Class ID : "+classIDNumber;
    }

    @Override
    public void updateViewGroup(ViewGroup vg){
        super.updateViewGroup(vg);

        // Battery
        final TextView tvbatt = (TextView) vg.findViewById(R.id.battery);

        // Logging interval
        final TextView tvlogi = (TextView) vg.findViewById(R.id.loggingInterval);

        //Global log count
        final TextView tvlogCount = (TextView) vg.findViewById(R.id.global_log_count);

        // Current Temperature
        final TextView tvtemplabel = (TextView) vg.findViewById(R.id.temperature_label_v13);
        final TextView tvtempvalue = (TextView) vg.findViewById(R.id.temperature_value_v13);

        //Current Humidity
        final TextView tvhumilabel = (TextView) vg.findViewById(R.id.humidity_label);
        final TextView tvhumivalue = (TextView) vg.findViewById(R.id.humidity_value);

        //Current Dewpoint
        final TextView tvdewplabel = (TextView) vg.findViewById(R.id.dewPoint_label);
        final TextView tvdewpvalue = (TextView) vg.findViewById(R.id.dewPoint_value);

        // Mode
        final TextView tvmode = (TextView) vg.findViewById(R.id.mode);

        //Units
        final TextView tvunits = (TextView) vg.findViewById(R.id.units);

        // Number of breaches
        final TextView tvnumb = (TextView) vg.findViewById(R.id.numBreach);

        // Set visible
        vg.findViewById(R.id.temperature_box_v13).setVisibility(View.VISIBLE);
        vg.findViewById(R.id.humidity_box).setVisibility(View.GONE);
        vg.findViewById(R.id.dewpoint_box).setVisibility(View.GONE);
        vg.findViewById(R.id.tables).setVisibility(View.VISIBLE);
        vg.findViewById(R.id.extras).setVisibility(View.VISIBLE);

        vg.findViewById(R.id.altitude).setVisibility(View.GONE);


        vg.setBackgroundColor(Color.WHITE);

        // Battery
        int battery = getBatteryLevel();
        if(battery >= 0 && battery <= 100){
            tvbatt.setVisibility(View.VISIBLE);
            tvbatt.setText("Battery: " + battery + "%");
        }

        //Log Interval
        int logInterval = getLoggingInterval();
        tvlogi.setText("Logging interval: " + logInterval+ " seconds");
        tvlogi.setVisibility(View.VISIBLE);

        // Log Count
        int logCount = getGlobalLogCount();
        tvlogCount.setText("Log count: " + globalLogCount);
        tvlogCount.setVisibility(View.VISIBLE);

        // Mode
        String modeText = "Mode: " +
                (isInAeroplaneMode() ? "\nFlight" : "Normal");
        tvmode.setVisibility(View.VISIBLE);
        tvmode.setText(modeText);

        //Units
        String unitsText = "Units: " +
                (isInFahrenheit() ? "Fahrenheit" : "Celsius") +
                " (" + getTempUnits() + ")";
        tvunits.setVisibility(View.VISIBLE);
        tvunits.setText(unitsText);
        String units = UNITSC;

        // Number of breaches
        tvnumb.setVisibility(View.VISIBLE);
        tvnumb.setText("No. of threshold breaches: " + getNumBreach());

        // Current Temperature
        tvtemplabel.setVisibility(View.VISIBLE);
        tvtemplabel.setText("TEMP");
        tvtempvalue.setVisibility(View.VISIBLE);
        tvtempvalue.setText("" + Utility.convertValueTo(
                String.valueOf(getCurrentTemperature(units)), getTempUnits()) + getTempUnits());

        // Current Humidity
        tvhumilabel.setVisibility(View.GONE);
        tvhumilabel.setText("HUMIDITY");
        tvhumivalue.setVisibility(View.GONE);
        tvhumivalue.setText("" + + getCurrentHumidity() + "%");

        // Current Dewpoint
        tvdewplabel.setVisibility(View.GONE);
        tvdewplabel.setText("DEWPNT");
        tvdewpvalue.setVisibility(View.GONE);
        tvdewpvalue.setText("" + Utility.convertValueTo(
                String.valueOf(getCurrentDewPoint(units)), getTempUnits()) + getTempUnits());




    }

    public void updateViewGroupForDetails(ViewGroup vg){

        String unitsForTemperature;
        String unitsForHumidity = "%";
        if ((int)getMode()> 100) {
            unitsForTemperature = UNITSF;
        } else {
            unitsForTemperature = UNITSC;
        }

        //Device Information
        TextView name = (TextView) vg.findViewById(R.id.deviceName);
        TextView address = (TextView) vg.findViewById(R.id.deviceAddress);
        TextView battery = (TextView) vg.findViewById(R.id.batteryLevel);
        TextView signal = (TextView) vg.findViewById(R.id.rssiStrength);
        TextView referenceDate = (TextView) vg.findViewById(R.id.referenceDate);
        TextView lastDownloadDate = (TextView) vg.findViewById(R.id.lastDownloadDate);
        TextView version = (TextView) vg.findViewById(R.id.versionNumber);
        TextView locked = (TextView) vg.findViewById(R.id.locked_unlock_label);
        TextView breachNumber = (TextView) vg.findViewById(R.id.breachCount);

        //Current Values
        TextView unitsForTemp = (TextView) vg.findViewById(R.id.unitsForTempDeviceInfo);
        TextView temperatureValue = (TextView) vg.findViewById(R.id.tempValue);
        TextView humidityValue = (TextView) vg.findViewById(R.id.humValue);
        TextView dewpointValue = (TextView) vg.findViewById(R.id.dewpValue);

        //Last 24 Hour Values
        TextView high24Temp = (TextView) vg.findViewById(R.id.high24TempValue);
        TextView avg24Temp = (TextView) vg.findViewById(R.id.avg24TempValue);
        TextView low24Temp = (TextView) vg.findViewById(R.id.low24TempValue);
        TextView high24Humi = (TextView) vg.findViewById(R.id.high24HumValue);
        TextView avg24Humi = (TextView) vg.findViewById(R.id.avg24HumValue);
        TextView low24Humi = (TextView) vg.findViewById(R.id.low24HumValue);
        TextView high24Dewp = (TextView) vg.findViewById(R.id.high24DewpValue);
        TextView avg24Dewp = (TextView) vg.findViewById(R.id.avg24DewpValue);
        TextView low24Dewp = (TextView) vg.findViewById(R.id.low24DewpValue);

        //Last Forever values
        TextView highestTemp = (TextView) vg.findViewById(R.id.highestTempValue);
        TextView lowestTemp = (TextView) vg.findViewById(R.id.lowestTempValue);
        TextView highestHumi = (TextView) vg.findViewById(R.id.highestHumValue);
        TextView lowestHumi = (TextView) vg.findViewById(R.id.lowestHumValue);

        //Set Values
        name.setText(getName());
        address.setText("Device ID: " + getAddress());
        battery.setText("Battery: " + getBatteryLevel() + "%");
        signal.setText("Signal: " + getRSSI() + "dB");
        version.setText("Version: " + getVersion());
        locked.setText(getLockedUnlocked());
        breachNumber.setText("Number of breaches: " +getNumBreach());

        if ((int)getMode() > 100) {
            unitsForTemp.setText("Fahrenheit");
        } else {
            unitsForTemp.setText("Celsius");
        }

        String temperatureForDisplay = String.format("%.1f", getCurrentTemperature(unitsForTemperature));
        String dewpointForDisplay = String.format("%.1f", getCurrentDewPoint(unitsForTemperature));
        temperatureValue.setText(temperatureForDisplay + unitsForTemperature);


        String highTempForDisplay = String.format("%.1f", getHighest24Temperature(unitsForTemperature));
        String avgTempForDisplay = String.format("%.1f", getAverage24Temperature(unitsForTemperature));
        String lowTempForDisplay = String.format("%.1f", getLowest24Temperature(unitsForTemperature));

        high24Temp.setText(highTempForDisplay + unitsForTemperature);
        avg24Temp.setText(avgTempForDisplay + unitsForTemperature);
        low24Temp.setText(lowTempForDisplay + unitsForTemperature);


        String highestTempForDisplay = String.format("%.1f", getHighestTemperature(unitsForTemperature));
        String lowestTempForDisplay = String.format("%.1f", getLowestTemperature(unitsForTemperature));

        highestTemp.setText(highestTempForDisplay + unitsForTemperature);
        lowestTemp.setText(lowestTempForDisplay + unitsForTemperature);

        referenceDate.setText("Reference Date : " + getReferenceDate());
        lastDownloadDate.setVisibility(View.GONE);

    }


    @Override
    public void setupChart(Chart chart, String command){
        if(!(chart instanceof BMLineChart)) return;
        BMLineChart lineChart = (BMLineChart) chart;
        if(command.equals("*bur")){
            isDownloadReady = !isDownloadReady;
            // If we sent the "*bur" command, setup the chart
            lineChart.init("", 5);
            lineChart.setLabels(
                    new String[]{
                            "Temperature (" + getTempUnits() + ")"
                    },
                    new int[]{
                            Color.RED
                    }
            );
        } else if(Pattern.matches("\\*units[c|f]", command)){
            // If we sent the "*units" command, change units
            boolean change = true;
            String oldUnits = getTempUnits();
            switch(command.charAt(command.length() - 1)){
                case 'c':   if(isInFahrenheit()) mode -= 100;   break;
                case 'f':   if(!isInFahrenheit()) mode += 100;  break;
                default:    change = false;                     break;
            }
            if(!change) return;
            int length = lineChart.getEntryCount() / 3;
            float[] temperature = new float[length];
            Log.d("BMTemp13", "Length: " + length);
            // Store the old temperatures
            for(int i = 0; i < length; i++){
                temperature[i] = lineChart.getEntry("Temperature (" + oldUnits + ")", i).getY();
            }
            // Manipulate the graph
            lineChart.setLabels(
                    new String[]{
                            "Temperature (" + getTempUnits() + ")"
                    },
                    new int[]{
                            Color.RED
                    }
            );
            // Manipulate the data
            for (int i = 0; i < length; i++) {
                temperature[i] = (isInFahrenheit())
                        ? temperature[i] * 1.8f + 32.0f
                        : (temperature[i] - 32.0f) / 1.8f;
            }
            // Add the temperatures back in
            for(int i = 0; i < length; i++){
                lineChart.addEntry("Temperature (" + getTempUnits() + ")", new Entry(i, temperature[i]));
            }
        } else if(Pattern.matches("\\*lint([0-9]+)", command)){
            Matcher matcher = Pattern.compile("\\*lint([0-9]+)").matcher(command);
            matcher.matches();
            int newLoggingInterval = Integer.valueOf(matcher.group(1).trim());
            BMDatabase database = BMDeviceMap.INSTANCE.getBMDatabase(getAddress());
            if(loggingInterval != newLoggingInterval){
                database.clear();
            }
            loggingInterval = newLoggingInterval;
            database.setLoggingInterval(loggingInterval);
        }
    }

    @Override
    public void updateChart(Chart chart, String text){
        if(!(chart instanceof BMLineChart)) return;
        BMLineChart lineChart = (BMLineChart) chart;
        // Only continue if it's THD values
        Matcher matcher = burst_pattern.matcher(text);
        if(!matcher.matches()) return;

        // Parse values
        float[] value = new float[3];
        for(int i = 0; i < value.length; i++) {
            value[i] = new Float(matcher.group(i + 1).trim());
        }
        // Insert values as new entries
        int index = lineChart.getEntryCount() / 3;
        lineChart.addEntry("Temperature (" + getTempUnits() + ")", new Entry(index, value[0]));
    }
}
