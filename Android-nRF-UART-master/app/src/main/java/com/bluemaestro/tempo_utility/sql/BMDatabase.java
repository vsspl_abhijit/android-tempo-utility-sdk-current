package com.bluemaestro.tempo_utility.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;

import com.bluemaestro.tempo_utility.CustomAdapter13;
import com.bluemaestro.tempo_utility.CustomAdapter23;
import com.bluemaestro.tempo_utility.CustomAdapter27;
import com.bluemaestro.tempo_utility.CustomAdapter42;
import com.bluemaestro.tempo_utility.Log;

import android.graphics.drawable.ColorDrawable;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.bluemaestro.tempo_utility.MainActivity;
import com.bluemaestro.tempo_utility.R;
import com.bluemaestro.tempo_utility.devices.BMPebble27;
import com.bluemaestro.tempo_utility.views.dialogs.BMProgressIndicator;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.data.Entry;
import com.bluemaestro.tempo_utility.UartService;
import com.bluemaestro.tempo_utility.ble.Utility;
import com.bluemaestro.tempo_utility.devices.BMDevice;
import com.bluemaestro.tempo_utility.sql.downloading.BMDownloader;
import com.bluemaestro.tempo_utility.sql.downloading.DownloadState;
import com.bluemaestro.tempo_utility.views.graphs.BMLineChart;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * Created by Garrett on 15/08/2016.
 * <p>
 * SQLite database for the data received by a particular device
 * <p>
 * Primary key is the time data received by the gateway
 */
public abstract class BMDatabase extends SQLiteOpenHelper implements Iterable<Cursor> {

    private ProgressBar progressBar1;

    private static final String TAG = "BMDatabase";

    /**
     * Version of this database
     */
    protected static final int DATABASE_VERSION = 2;

    /**
     * Database name
     */
    protected final String DATABASE_NAME;

    /**
     * Table names
     */
    protected static final String TABLE_INFO = "info";
    protected static final String TABLE_DATA = "data";

    /**
     * Key names
     */
    protected static final String KEY_ADDR = "address";
    protected static final String KEY_NAME = "name";
    protected static final String KEY_VERS = "version";
    protected static final String KEY_LAST_POINTER = "last_pointer";
    protected static final String KEY_MODE = "mode";
    protected static final String KEY_LAST_DOWNLOAD = "last_download";
    protected static final String KEY_THRESHOLDS = "thresholds";

    protected static final String KEY_TIME = "time";
    protected static final String KEY_INDEX = "log_index";

    /**
     * Instruction to create the info table, as a String
     */
    private final String CREATE_TABLE_INFO =
            "CREATE TABLE " + TABLE_INFO + "(" +
                    KEY_ADDR + " TEXT PRIMARY KEY" + "," +
                    KEY_NAME + " TEXT" + "," +
                    KEY_VERS + " INTEGER" + "," +
                    KEY_LAST_POINTER + " INTEGER" + "," +
                    KEY_MODE + " INTEGER" + "," +
                    KEY_LAST_DOWNLOAD + " TEXT" + "," +
                    KEY_THRESHOLDS + " TEXT" + ")";

    private BMDevice bmDevice;

    private int loggingInterval;

    BMProgressIndicator progress;

    public int getLoggingInterval() {
        return loggingInterval;
    }

    public void setLoggingInterval(int loggingInterval) {
        this.loggingInterval = loggingInterval;
    }

    /**
     * Instruction to create the data table, as a String
     * This should be modified by subclasses, in order to
     * add the required data for that Blue Maestro device
     */
    protected String CREATE_TABLE_DATA =
            "CREATE TABLE " + TABLE_DATA + "(" +
                    KEY_INDEX + " INTEGER PRIMARY KEY";

    /*************************************************************/

    /**
     * Constructor for writing to a database
     *
     * @param context
     * @param bmDevice
     */
    public BMDatabase(Context context, BMDevice bmDevice) {
        super(context, bmDevice.getAddress(), null, DATABASE_VERSION);
        this.bmDevice = bmDevice;
        //this.loggingInterval = 3600;
        this.DATABASE_NAME = bmDevice.getAddress();
    }

    /**
     * Constructor for reading from a database
     *
     * @param context
     * @param address
     */
    public BMDatabase(Context context, String address) {
        super(context, address, null, DATABASE_VERSION);
        this.DATABASE_NAME = address;
    }

    /**
     * Creates the database
     *
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate - " + getClass().getSimpleName());
        db.execSQL(CREATE_TABLE_INFO);
        db.execSQL(CREATE_TABLE_DATA + ")");

        if (bmDevice != null) {
            Log.d(TAG, "onCreate - " + TABLE_INFO);
            ContentValues values = new ContentValues();
            values.put(KEY_ADDR, bmDevice.getAddress());
            values.put(KEY_NAME, bmDevice.getName());
            values.put(KEY_VERS, bmDevice.getVersion());
            values.put(KEY_LAST_POINTER, 0);
            values.put(KEY_MODE, bmDevice.getMode());
            values.put(KEY_LAST_DOWNLOAD, "N/A");

            db.insert(TABLE_INFO, null, values);
        }
    }

    /**
     * Upgrades the database to a new version
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "onUpgrade - " + getClass().getSimpleName() +
                " from v" + oldVersion + " to v" + newVersion);
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INFO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DATA);

        // Create tables again
        onCreate(db);
    }

    /*************************************************************/

    public abstract ContentValues addToValues(ContentValues values, String key, double data);

    /**
     * Download data
     *
     * @param service    UART service to start downloading from
     * @param downloader Downloader to use
     * @return State of the download. See DownloadState for more information on this
     * @throws UnsupportedEncodingException
     * @see DownloadState
     */
    public abstract DownloadState downloadData(UartService service, BMDownloader downloader, BMProgressIndicator progress) throws UnsupportedEncodingException;

    /**
     * Download data
     *
     * @param service    UART service to start downloading from
     * @param downloader Downloader to use
     * @param keys       Keys in table to use
     * @param clearData  Whether "*logall" should be sent only, and the previous data deleted
     * @throws UnsupportedEncodingException
     */
    protected final DownloadState downloadData(UartService service, BMDownloader downloader, String[] keys, boolean clearData, String referenceDate, BMProgressIndicator progress) throws UnsupportedEncodingException {
        if (keys.length < 1) return DownloadState.FAILURE_NOT_DEFINED;
        Log.d(TAG, "Keys length is : " + keys.length);
        // Get the old last pointer
        int oldPointer = getLastPointer();
        Log.d(TAG, "Previous last pointer: " + oldPointer);
        Log.d(TAG, "Reference Date is: " + referenceDate);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
        Calendar cal = Calendar.getInstance();
        Date referenceDateDate;
        String timeStampForRecord;

        //while (downloader.isWaitingForBluetooth());

        // Download the data
        downloader.clear();
        // Note: currently, just send "*logall"
        // However, ability to send "*logallX" should be possible
        if (clearData) {
            clear();
            Utility.sendCommand(service, "*logall");
            Log.d(TAG, "Trying to pass command *logall");
        } else {
            Utility.sendCommand(service, "*logall" + oldPointer);
        }

        //In while loop under downloader passes false for isDownloading
        while (downloader.isDownloading()) {
            //Log.d("BMDownloader", "Threshold No.: " + downloader.getIndex());
        }
        if (downloader.isdownloadFailure()) {
            return DownloadState.FAILURE_NOT_DEFINED;
        }
        double[][] data = downloader.getData();
        //progress.newProgressIndicator();

        Log.d(TAG, "Data length is : " + data.length);
        if (data.length != keys.length) return DownloadState.FAILURE_NOT_DEFINED;

        // Save the last pointer for future use
        int newPointer = downloader.getLastPointer();
        byte mode = downloader.getMode();

        // Get writable database
        // Get "now" date
        SQLiteDatabase db = getWritableDatabase();

        Timestamp now = TIMESTAMP_NOW();

        String date = getLastDownloadDate();

        /* Get start date from referenceDate or if this is null, take current time and work out when
         * referenceDate should be by multiplying logging interval by the number of records and taking
         * this from current time.
         */

        if (referenceDate != "null") {
            try {
                referenceDateDate = dateFormat.parse(referenceDate);
            } catch (ParseException e) {
                e.printStackTrace();
                cal.setTime(new Date());
                cal.add(Calendar.SECOND, -(getLoggingInterval() * data[0].length));
                referenceDateDate = cal.getTime();
            }
        } else {
            cal.setTime(new Date());
            cal.add(Calendar.SECOND, -(getLoggingInterval() * data[0].length));
            referenceDateDate = cal.getTime();
        }

        // Update table info
        ContentValues values = new ContentValues();
        values.put(KEY_ADDR, getAddress());
        values.put(KEY_NAME, getName());
        values.put(KEY_VERS, getVersion());
        values.put(KEY_LAST_POINTER, newPointer);
        values.put(KEY_MODE, mode);
        values.put(KEY_LAST_DOWNLOAD, now.toString());
        values.put(KEY_THRESHOLDS, getThresholdsAsString(downloader, mode));
        db.update(TABLE_INFO, values, KEY_ADDR + "=?", new String[]{getAddress()});

        // New data
        for (int i = 0; i < data[0].length; i++) {
            values = new ContentValues();
            timeStampForRecord = getTimeStampForRecord(getLoggingInterval(), i, referenceDateDate, cal, dateFormat);
            Log.d("BMDatabase", "TimeStamp for Record: " + timeStampForRecord);
            values.put(KEY_INDEX, i);
            values.put(KEY_TIME, timeStampForRecord);
            for (int j = 0; j < keys.length; j++) {
                values = addToValues(values, keys[j], data[j][i]);
            }
            db.insert(TABLE_DATA, null, values);
            progress.setProgressReferenceDate(i);

        }
        db.close();
        return DownloadState.SUCCESS;
    }


    /* This works out the timestamp for each record based on the reference date, the logging interval and the record number
     */
    private String getTimeStampForRecord(int loggingInterval, int recordNumber, Date referenceDate, Calendar cal, SimpleDateFormat dateFormat) {
        cal.setTime(referenceDate);
        cal.add(Calendar.SECOND, loggingInterval * recordNumber);
        Date timeStampDate = cal.getTime();
        return dateFormat.format(timeStampDate);
    }

    private final String getThresholdsAsString(BMDownloader downloader, byte mode) {
        String thresholdsString = "";
        short[] thresholds = downloader.getThresholds();
        int[] thresholdTypes = downloader.getThresholdTypes();
        for (int i = 0; i < thresholds.length; i++) {
            double value = thresholds[i];
            int type = thresholdTypes[i];
            switch (type) {
                case 0: // No threshold
                    // Don't concatenate threshold
                    break;
                case 1: // Temperature
                case 3: // Dew Point
                    // Change to Fahrenheit if need be
                    if (mode >= 100) value = value * 1.8 + 32;
                    // Fallthrough
                case 2: // Humidity
                    // Divide by 10 (short -> double)
                    value /= 10.0;
                    // Fallthrough
                default:
                    // Concatenate to string
                    thresholdsString += value + ",";
                    break;
            }
        }
        if (thresholdsString.length() != 0) {
            thresholdsString = thresholdsString.substring(0, thresholdsString.length() - 1);
        }
        return thresholdsString;
    }

    private synchronized final void waitForDownload(BMDownloader downloader) {
        while (downloader.isDownloading()) try {
            wait(10000); // Wait for 10 seconds
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized final void completedDownload() {
        notifyAll();
    }

    /*************************************************************/

    /**
     * Get the number of data within this database
     *
     * @return Number of pieces of data
     */
    public final int getDataCount() {
        SQLiteDatabase db = getReadableDatabase();
        long count = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM " + TABLE_DATA, null);
        return (int) count;
    }

    /**
     * Get the cursor pointing to our data
     *
     * @param primaryKey The primary key of the data wanted
     * @param labels     The names of the columns, in order
     * @return Cursor pointing to our data
     */
    public final Cursor getCursor(String table, String primaryKey, String primaryValue, String[] labels) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(
                table,
                labels,
                primaryKey + "=?",
                new String[]{primaryValue},
                null, null, null, null
        );
        if (cursor != null) cursor.moveToFirst();
        return cursor;
    }

    public final Cursor getCursorHead(String table) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursorHeader = db.query(table, new String[]{"*"}, null, null, null, null, null, null);
        if (cursorHeader != null) cursorHeader.moveToFirst();
        return cursorHeader;
    }

    public final void clear() {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ADDR, bmDevice.getAddress());
        values.put(KEY_LAST_POINTER, 0);
        values.put(KEY_LAST_DOWNLOAD, "N/A");
        db.update(TABLE_INFO, values, KEY_ADDR + "=?", new String[]{getAddress()});
        db.delete(TABLE_DATA, null, null);
    }

    /*************************************************************/

    public final String getAddress() {
        return DATABASE_NAME;
    }

    public final String getName() {
        Cursor cursor = getCursor(TABLE_INFO, KEY_ADDR, DATABASE_NAME,
                new String[]{
                        KEY_ADDR,
                        KEY_NAME,
                        KEY_VERS,
                        KEY_LAST_POINTER,
                        KEY_MODE,
                        KEY_LAST_DOWNLOAD,
                        KEY_THRESHOLDS
                });
        String name = cursor.getString(1);
        cursor.close();
        return name;
    }

    public final byte getVersion() {
        Cursor cursor = getCursor(TABLE_INFO, KEY_ADDR, DATABASE_NAME,
                new String[]{
                        KEY_ADDR,
                        KEY_NAME,
                        KEY_VERS,
                        KEY_LAST_POINTER,
                        KEY_MODE,
                        KEY_LAST_DOWNLOAD,
                        KEY_THRESHOLDS
                });
        byte version = Byte.parseByte(cursor.getString(2));
        cursor.close();
        return version;
    }

    public final int getLastPointer() {
        Cursor cursor = getCursor(TABLE_INFO, KEY_ADDR, DATABASE_NAME,
                new String[]{
                        KEY_ADDR,
                        KEY_NAME,
                        KEY_VERS,
                        KEY_LAST_POINTER,
                        KEY_MODE,
                        KEY_LAST_DOWNLOAD,
                        KEY_THRESHOLDS
                });
        int lastPointer = Integer.parseInt(cursor.getString(3));
        cursor.close();
        return lastPointer;
    }

    public final byte getMode() {
        Cursor cursor = getCursor(TABLE_INFO, KEY_ADDR, DATABASE_NAME,
                new String[]{
                        KEY_ADDR,
                        KEY_NAME,
                        KEY_VERS,
                        KEY_LAST_POINTER,
                        KEY_MODE,
                        KEY_LAST_DOWNLOAD,
                        KEY_THRESHOLDS
                });
        byte mode = Byte.parseByte(cursor.getString(4));
        cursor.close();
        return mode;
    }

    public final String getLastDownloadDate() {
        Cursor cursor = getCursor(TABLE_INFO, KEY_ADDR, DATABASE_NAME,
                new String[]{
                        KEY_ADDR,
                        KEY_NAME,
                        KEY_VERS,
                        KEY_LAST_POINTER,
                        KEY_MODE,
                        KEY_LAST_DOWNLOAD,
                        KEY_THRESHOLDS
                });
        String lastDownload = cursor.getString(5);
        cursor.close();
        return lastDownload;
    }

    public final double[] getThresholds() {
        Cursor cursor = getCursor(TABLE_INFO, KEY_ADDR, DATABASE_NAME,
                new String[]{
                        KEY_ADDR,
                        KEY_NAME,
                        KEY_VERS,
                        KEY_LAST_POINTER,
                        KEY_MODE,
                        KEY_LAST_DOWNLOAD,
                        KEY_THRESHOLDS
                });
        String thresholdsString = cursor.getString(6);
        if (thresholdsString == null || thresholdsString.equals("")) return new double[0];
        String[] thresholds = thresholdsString.split(",");
        double[] values = new double[thresholds.length];
        for (int i = 0; i < thresholds.length; i++) {
            String threshold = thresholds[i];
            double value = new Double(threshold);
            values[i] = value;
        }
        return values;
    }

    /*************************************************************/

    public void displayAsInfo(ArrayAdapter<String> listAdapter) {
        listAdapter.clear();
        listAdapter.add("Name: " + getName());
        listAdapter.add("Address: " + getDatabaseName());
        listAdapter.add("Version: " + getVersion());
        listAdapter.add("Last downloaded: " + getLastDownloadDate());
        listAdapter.add("");
        listAdapter.add("Number of records: " + getDataCount());

        listAdapter.notifyDataSetChanged();
    }

    /**
     * Display this database as a chart
     *
     * @param chart The chart to display to
     */
    public abstract void displayAsChart(Chart chart, BMProgressIndicator progress);

    protected boolean displayAsChart(int version, BMLineChart lineChart, String[] keys, String[] tempKeys, int[] colours, BMProgressIndicator progress) {

        List<String> tempList = Arrays.asList(tempKeys);
        switch (version) {
            case 13:

                // List of temperature keys

                for (int i = 0; i < keys.length; i++) {
                    if (tempList.contains(keys[i])) keys[i] += " (" + getTempUnits() + ")";
                    keys[i] = Utility.formatKey(keys[i]);
                }
                for (int i = 0; i < tempKeys.length; i++) {
                    tempKeys[i] += " (" + getTempUnits() + ")";
                    tempKeys[i] = Utility.formatKey(tempKeys[i]);
                }

                lineChart.init("", 5);
                lineChart.setLabels(keys, colours);
                progress.setMax(getDataCount());

                for (Cursor cursor : this) {
                    // Insert values as new entries
                    int index = lineChart.getEntryCount() / keys.length;
                    for (int i = 0; i < keys.length; i++) {
                        String value = cursor.getString(i + 1);
                        String key = keys[i];
                        if (isInFahrenheit() && tempList.contains(key)) {
                            value = Utility.convertValueTo(value, getTempUnits());
                        }
                        float val = value != null ? new Float(value) : 0;
                        lineChart.addEntry(key, new Entry(index, val));
                    }
                    progress.setProgress(index);
                }
                break;

            case 22:
            case 23:
            case 27:

                // List of temperature keys
                for (int i = 0; i < keys.length; i++) {
                    if (tempList.contains(keys[i])) keys[i] += " (" + getTempUnits() + ")";
                    keys[i] = Utility.formatKey(keys[i]);
                }
                for (int i = 0; i < tempKeys.length; i++) {
                    tempKeys[i] += " (" + getTempUnits() + ")";
                    tempKeys[i] = Utility.formatKey(tempKeys[i]);
                }

                lineChart.init("", 5);
                lineChart.setLabels(keys, colours);


                for (Cursor cursor : this) {
                    // Insert values as new entries
                    int index = lineChart.getEntryCount() / keys.length;
                    for (int i = 0; i < keys.length; i++) {
                        String value = cursor.getString(i + 1);
                        String key = keys[i];
                        if (isInFahrenheit() && tempList.contains(key)) {
                            value = Utility.convertValueTo(value, getTempUnits());
                        }
                        float val = value != null ? new Float(value) : 0;
                        lineChart.addEntry(key, new Entry(index, val));
                    }
                    progress.setProgress(index);
                }
                break;


            case 32:
                lineChart.init("", 5);
                lineChart.setLabels(keys, colours);


                for (Cursor cursor : this) {
                    // Insert values as new entries
                    int index = lineChart.getEntryCount() / keys.length;
                    for (int i = 0; i < keys.length; i++) {
                        String value = cursor.getString(i + 1);
                        String key = keys[i];
                        float val = value != null ? new Float(value) : 0;
                        lineChart.addEntry(key, new Entry(index, val));
                    }
                    progress.setProgress(index);
                }

                break;

            case 42:
                lineChart.init("", 5);
                lineChart.setLabels(keys, colours);


                for (Cursor cursor : this) {
                    // Insert values as new entries
                    int index = lineChart.getEntryCount() / keys.length;
                    for (int i = 0; i < keys.length; i++) {
                        String value = cursor.getString(i + 1);
                        String key = keys[i];
                        float val = value != null ? new Float(value) : 0;
                        lineChart.addEntry(key, new Entry(index, val));
                    }
                    progress.setProgress(index);
                }

        }

        // Thresholds
        double[] thresholds = getThresholds();
        for (int i = 0; i < thresholds.length; i++) {
            lineChart.addThresholdLine((float) thresholds[i], "Threshold " + (i + 1), Color.MAGENTA);
        }

        lineChart.setPinchZoom(true);
        lineChart.setHorizontalScrollBarEnabled(true);

        lineChart.enableScroll();
        lineChart.setTouchEnabled(true);
        lineChart.setDragEnabled(true);

        return true;
    }

    /**
     * Display this database as a table
     *
     * @param context  The application context
     * @param listView The table to display to
     */
    public abstract void displayAsTable(Context context, ListView listView);


    /**
     * Display this database as a table
     *
     * @param context  The application context
     * @param listView The view that will display to
     * @param version  The version of the device that is being displayed
     * @param units    The units of measure that the temperature values need to be displayed as
     */
    protected void displayAsTable(Context context, ListView listView, Integer version, String units) {

        switch (version) {

            case 13:

                ArrayList<BMTempDatabase13.Readings> listOfReadings13 = new ArrayList<BMTempDatabase13.Readings>();

                for (Cursor cursor : this) {
                    BMTempDatabase13.Readings readingsFromDatabase = new BMTempDatabase13.Readings();
                    readingsFromDatabase.setIndexValue(cursor.getInt(0));
                    readingsFromDatabase.setTemperatureValue(cursor.getFloat(1));
                    readingsFromDatabase.setDateStamp(cursor.getString(2));
                    listOfReadings13.add(readingsFromDatabase);
                }
                CustomAdapter13 readingsAdapter13 = new CustomAdapter13(context, listOfReadings13, units);
                listView.setAdapter(readingsAdapter13);
                break;

            case 14:


            case 22:
            case 23:

                ArrayList<BMTempHumiDatabase23.Readings> listOfReadings23 = new ArrayList<BMTempHumiDatabase23.Readings>();

                for (Cursor cursor : this) {
                    BMTempHumiDatabase23.Readings readingsFromDatabase = new BMTempHumiDatabase23.Readings();
                    readingsFromDatabase.setIndexValue(cursor.getInt(0));
                    readingsFromDatabase.setTemperatureValue(cursor.getFloat(1));
                    readingsFromDatabase.setHumidityValue(cursor.getFloat(2));
                    readingsFromDatabase.setDewpointValue(cursor.getFloat(3));
                    readingsFromDatabase.setDateStamp(cursor.getString(4));
                    listOfReadings23.add(readingsFromDatabase);
                }
                CustomAdapter23 readingsAdapter23 = new CustomAdapter23(context, listOfReadings23, units);
                listView.setAdapter(readingsAdapter23);
                break;

            case 27:

                ArrayList<BMPebbleDatabase27.Readings> listOfReadings27 = new ArrayList<BMPebbleDatabase27.Readings>();

                for (Cursor cursor : this) {
                    BMPebbleDatabase27.Readings readingsFromDatabase = new BMPebbleDatabase27.Readings();
                    readingsFromDatabase.setIndexValue(cursor.getInt(0));
                    readingsFromDatabase.setTemperatureValue(cursor.getFloat(1));
                    readingsFromDatabase.setHumidityValue(cursor.getFloat(2));
                    readingsFromDatabase.setPressureValue(cursor.getFloat(3));
                    readingsFromDatabase.setDewpointValue(10);
                    readingsFromDatabase.setDateStamp(cursor.getString(4));
                    listOfReadings27.add(readingsFromDatabase);
                }
                CustomAdapter27 readingsAdapter27 = new CustomAdapter27(context, listOfReadings27, units);
                listView.setAdapter(readingsAdapter27);
                break;

            case 32:
                break;

            case 42:

                ArrayList<BMButtonDatabase42.Readings> listOfReadings42 = new ArrayList<BMButtonDatabase42.Readings>();

                for (Cursor cursor : this) {
                    BMButtonDatabase42.Readings readingsFromDatabase = new BMButtonDatabase42.Readings();
                    readingsFromDatabase.setIndexValue(cursor.getInt(0));
                    readingsFromDatabase.setButtonCountValue(cursor.getFloat(1));
                    readingsFromDatabase.setDateStamp(cursor.getString(2));
                    listOfReadings42.add(readingsFromDatabase);
                }
                CustomAdapter42 readingsAdapter42 = new CustomAdapter42(context, listOfReadings42, units);
                listView.setAdapter(readingsAdapter42);
                Collections.reverse(listOfReadings42);
                break;




        }
    }


    /**
     * Export this database to a .csv file
     * Exports to: "/sdcard/address-of-device"
     *
     * @param filename Name of the file
     * @throws IOException
     */
    public abstract File export(Context context, String filename) throws IOException;

    /**
     * Export this database to a .csv file
     *
     * @throws IOException
     */
    protected File export(Context context, String filename, Integer version, String units) throws IOException {
        File file = new File(filename);
        file.createNewFile();
        // List of temperature keys

        // Setting content to UTF-8 with BOM (for degree symbol)
        OutputStream os = new FileOutputStream(filename);
        os.write(239);
        os.write(187);
        os.write(191);

        CSVWriter writer = new CSVWriter(new PrintWriter(new OutputStreamWriter(os, "UTF-8")), ',');
        Cursor cursorHead = getCursorHead(TABLE_INFO);
        String[] head = new String[cursorHead.getColumnCount()];
        String[] data = new String[cursorHead.getColumnCount()];

        // Table info columns and row
        for (int i = 0; i < cursorHead.getColumnCount(); i++) {
            head[i] = cursorHead.getColumnName(i);
            data[i] = cursorHead.getString(i);
        }
        writer.writeNext(head);
        writer.writeNext(data);

        // Table data columns
        head = new String[cursorHead.getColumnCount()];
        cursorHead = getCursorHead(TABLE_DATA);
        for (int i = 0; i < cursorHead.getColumnCount(); i++) {
            head[i] = cursorHead.getColumnName(i);
            //if(tempList.contains(head[i])) head[i] += " (" + getTempUnits() + ")";
        }
        writer.writeNext(head);
        cursorHead.close();
        int keys = 3;
        // Table data rows
        for (Cursor cursor : this) {
            for (int i = 0; i < cursor.getColumnCount(); i++) {
                data[i] = cursor.getString(i);
                /*if(i == 0) continue;
                String key = keys[i - 1];
                if(isInFahrenheit() && tempList.contains(key)) {
                    data[i] = Utility.convertValueTo(data[i], getTempUnits());
                }
                */
                Log.d("BMDatabase", "Cursor iteration and data is " + data[i]);
            }
            writer.writeNext(data);
        }
        writer.close();
        return file;
    }

    /*************************************************************/

    /**
     * Timestamp now
     *
     * @return
     */
    public static final Timestamp TIMESTAMP_NOW() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date date = new Date();
        return Timestamp.valueOf(dateFormat.format(date));
    }

    protected final boolean isInFahrenheit() {
        return getMode() >= 100;
    }

    protected final String getTempUnits() {
        return isInFahrenheit() ? "°F" : "°C";
    }

    /*************************************************************/

    @Override
    public Iterator<Cursor> iterator() {
        return new BMDatabaseIterator();
    }

    /**
     * Custom iterator over Cursor objects for Blue Maestro databases
     */
    private final class BMDatabaseIterator implements Iterator<Cursor> {

        private SQLiteDatabase db;
        private Cursor cursor;
        private int index;

        public BMDatabaseIterator() {
            this.db = getReadableDatabase();
            this.cursor = db.query(TABLE_DATA, new String[]{"*"}, null, null, null, null, null, null);
            if (cursor != null) cursor.moveToFirst();
            cursor.moveToPrevious();
        }

        @Override
        public void finalize() {
            cursor.close();
        }

        @Override
        public boolean hasNext() {
            boolean hasNext = cursor.moveToNext();
            cursor.moveToPrevious();
            return hasNext;
        }

        @Override
        public Cursor next() {
            cursor.moveToNext();
            return cursor;
        }

        @Override
        public void remove() {

        }
    }

    public boolean displayStoredDataAsChart(BMLineChart lineChart, String[] keys, String[] tempKeys, int[] colours, int version, BMProgressIndicator progress) {

        List<String> tempList = Arrays.asList(tempKeys);
        float tempValueForDewP = 0;
        float humiValueForDewP = 0;
        float dewPValue = 0;
        int buttonValue = 0;
        //progress.setMax(getDataCount());

        switch (version) {

            case 13:
                // List of temperature keys

                for (int i = 0; i < keys.length; i++) {
                    if (tempList.contains(keys[i])) keys[i] += " (" + getTempUnits() + ")";
                    keys[i] = Utility.formatKey(keys[i]);
                }
                for (int i = 0; i < tempKeys.length; i++) {
                    tempKeys[i] += " (" + getTempUnits() + ")";
                    tempKeys[i] = Utility.formatKey(tempKeys[i]);
                }
                lineChart.init("", 5);
                lineChart.setLabels(keys, colours);

                for (Cursor cursor : this) {
                    // Insert values as new entries
                    int index = lineChart.getEntryCount() / keys.length;
                    for (int i = 0; i < keys.length; i++) {
                        String value = cursor.getString(i + 1);
                        String key = keys[i];
                        if (isInFahrenheit() && tempList.contains(key)) {
                            value = Utility.convertValueTo(value, getTempUnits());
                        }
                        float val = value != null ? new Float(value) : 0;
                        lineChart.addEntry(key, new Entry(index, val));

                    }
                    //progress.setProgress(index);
                }


                break;

            case 23:
            case 17:
            case 22:
            case 32:
                // List of temperature keys

                for (int i = 0; i < keys.length; i++) {
                    if (tempList.contains(keys[i])) keys[i] += " (" + getTempUnits() + ")";
                    keys[i] = Utility.formatKey(keys[i]);
                }
                for (int i = 0; i < tempKeys.length; i++) {
                    tempKeys[i] += " (" + getTempUnits() + ")";
                    tempKeys[i] = Utility.formatKey(tempKeys[i]);
                }
                lineChart.init("", 5);
                lineChart.setLabels(keys, colours);

                for (Cursor cursor : this) {
                    // Insert values as new entries
                    int index = lineChart.getEntryCount() / keys.length;
                    for (int i = 0; i < keys.length; i++) {
                        String value = cursor.getString(i + 1);
                        String key = keys[i];
                        if (isInFahrenheit() && tempList.contains(key)) {
                            value = Utility.convertValueTo(value, getTempUnits());
                        }
                        float val = value != null ? new Float(value) : 0;
                        lineChart.addEntry(key, new Entry(index, val));

                    }
                    //progress.setProgress(index);
                }


                break;

            case 27:
                // List of temperature keys
                for (int i = 0; i < tempKeys.length; i++) {
                    if (tempList.contains(keys[i])) keys[i] += " (" + getTempUnits() + ")";
                    keys[i] = Utility.formatKey(keys[i]);
                }
                for (int i = 0; i < tempKeys.length; i++) {
                    tempKeys[i] += " (" + getTempUnits() + ")";
                    tempKeys[i] = Utility.formatKey(tempKeys[i]);
                }
                lineChart.init("", 5);
                lineChart.setLabels(keys, colours);

                for (Cursor cursor : this) {
                    // Insert values as new entries
                    int index = lineChart.getEntryCount() / keys.length;
                    for (int i = 0; i < keys.length; i++) {

                        String value;
                        String key = keys[i];

                        if (i < keys.length - 1) {
                            value = cursor.getString(i + 1);
                            if (isInFahrenheit() && tempList.contains(key)) {
                                value = Utility.convertValueTo(value, getTempUnits());
                            }
                            float val = value != null ? new Float(value) : 0;

                            if (i == 0) {
                                tempValueForDewP = val;
                            }
                            if (i == 1) {
                                humiValueForDewP = val;
                            }
                            lineChart.addEntry(key, new Entry(index, val));
                        }
                        if (i == keys.length - 1){
                            dewPValue = tempValueForDewP - ((100 - humiValueForDewP)/5);
                            lineChart.addEntry(key, new Entry(index, dewPValue));
                        }
                    }
                }
                break;

            case 42:
                lineChart.init("", 5);
                lineChart.setLabels(keys, colours);

                for (Cursor cursor : this) {
                    // Insert values as new entries
                    int index = lineChart.getEntryCount() / keys.length;
                    for (int i = 0; i < keys.length; i++) {

                        String value;
                        String key = keys[i];

                        if (i == keys.length - 1) {
                            value = cursor.getString(i + 1);
                            float val = new Float(value);
                            lineChart.addEntry(key, new Entry(index, val));
                            Log.d("BMDatabase", "Cursor iteration and data is " + value);
                        }
                    }
                }
                break;



        }
        // Thresholds
        double[] thresholds = getThresholds();
        for (int i = 0; i < thresholds.length; i++) {
            lineChart.addThresholdLine((float) thresholds[i], "Threshold " + (i + 1), Color.MAGENTA);
        }

        lineChart.setPinchZoom(true);
        lineChart.setHorizontalScrollBarEnabled(true);

        lineChart.enableScroll();
        lineChart.setTouchEnabled(true);
        lineChart.setDragEnabled(true);
        return true;

    }


    public boolean setUpChartForStoredData(Chart chart, Integer version, BMProgressIndicator progress) {

        final String KEY_TEMP = "temperature";
        final String KEY_HUMI = "humidity";
        final String KEY_DEWP = "dew_point";
        final String KEY_PRESS = "pressure";
        final String KEY_BUTTON = "button";

        boolean finished = false;
        BMLineChart lineChart = (BMLineChart) chart;


        switch (version) {

            case 13:
                finished = displayStoredDataAsChart(
                        lineChart,
                        new String[] {
                                KEY_TEMP,
                        },
                        new String[] {
                                KEY_TEMP,
                        },
                        new int[] {
                                Color.RED,
                        },
                        version,
                        progress
                );

                break;

            case 22:
            case 23:

                finished = displayStoredDataAsChart(
                        lineChart,
                        new String[]{
                                KEY_TEMP,
                                KEY_HUMI,
                                KEY_DEWP
                        },
                        new String[]{
                                KEY_TEMP,
                                KEY_DEWP
                        },
                        new int[]{
                                Color.RED,
                                Color.BLUE,
                                Color.GREEN
                        },
                        version,
                        progress
                );
                break;

            case 27:

                finished = displayStoredDataAsChart(
                        lineChart,
                        new String[]{
                                KEY_TEMP,
                                KEY_HUMI,
                                KEY_PRESS,
                                KEY_DEWP
                        },
                        new String[]{
                                KEY_TEMP,
                                KEY_DEWP
                        },
                        new int[]{
                                Color.RED,
                                Color.BLUE,
                                Color.MAGENTA,
                                Color.GREEN
                        },
                        version,
                        progress
                );
                break;


            case 32:
                final String KEY_CHANNEL_ONE = "channel_one";
                final String KEY_CHANNEL_TWO = "channel_two";

                finished = displayStoredDataAsChart(
                        lineChart,
                        new String[]{
                                KEY_CHANNEL_ONE,
                                KEY_CHANNEL_TWO,
                        },
                        new String[]{
                        },
                        new int[]{
                                Color.RED,
                                Color.BLUE,
                        },
                        version,
                        progress
                );
                break;

            case 42:

                finished = displayStoredDataAsChart(
                        lineChart,
                        new String[]{
                                KEY_BUTTON
                        },
                        new String[]{
                        },
                        new int[]{
                                Color.GREEN
                        },
                        version,
                        progress
                );
                break;

        }
        return finished;

    }

    public void setUpTableForStoredData(Context context, ListView listView, Integer version, Byte units) {
        String unitsOfMeasure;

        if ((int)units > 100) {
            unitsOfMeasure = "º F";
        } else {
            unitsOfMeasure = "º C";
        }
        displayAsTable(context, listView, version, unitsOfMeasure);

    }

    public File setUpExportForStoredData(Context context, String filename, Integer version, Integer mode) throws IOException {
        String unitsOfMeasure;

        if (mode > 100) {
            unitsOfMeasure = " ºF";
        } else {
            unitsOfMeasure = " ºC";
        }

        return export(context, filename, version, unitsOfMeasure);
    }
}
