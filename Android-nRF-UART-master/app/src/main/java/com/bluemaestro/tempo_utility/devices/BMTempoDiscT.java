package com.bluemaestro.tempo_utility.devices;

import android.bluetooth.BluetoothDevice;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.Chart;
import com.bluemaestro.tempo_utility.R;

/**
 * Created by Garrett on 05/08/2016.
 *
 * Blue Maesttro Tempo Disc T
 */
public class BMTempoDiscT extends BMDevice {

    private double temperature;

    public BMTempoDiscT(BluetoothDevice device, byte id) {
        super(device, id);
    }

    @Override
    public void updateWithData(int rssi, String name, byte[] mData, byte[] sData){
        super.updateWithData(rssi, name, mData, sData);
        // Conversion from unsigned to signed, then dividing by 10.0 for degrees
        temperature = convertToInt16(mData[7], mData[6]) / 10.0;
    }

    @Override
    public void setupChart(Chart chart, String command) {

    }

    @Override
    public void updateChart(Chart chart, String text) {

    }

    public void updateViewGroupForDetails(ViewGroup vg) {

    }

    public double getTemperature(){
        return temperature;
    }

    @Override
    public void updateViewGroup(ViewGroup vg){
        super.updateViewGroup(vg);
        final TextView tvtemp = (TextView) vg.findViewById(R.id.temperature_label);
        tvtemp.setVisibility(View.VISIBLE);
        tvtemp.setText("Temperature = " + getTemperature() + "°C");
    }
}
