package com.bluemaestro.tempo_utility.sql;

import android.content.ContentValues;
import android.content.Context;
import android.widget.ListView;

import com.bluemaestro.tempo_utility.views.dialogs.BMProgressIndicator;
import com.github.mikephil.charting.charts.Chart;
import com.bluemaestro.tempo_utility.UartService;
import com.bluemaestro.tempo_utility.devices.BMDevice;
import com.bluemaestro.tempo_utility.sql.downloading.BMDownloader;
import com.bluemaestro.tempo_utility.sql.downloading.DownloadState;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by Garrett on 25/08/2016.
 */
public final class BMDefaultDatabase extends BMDatabase {

    public BMDefaultDatabase(Context context, BMDevice bmDevice) {
        super(context, bmDevice);
    }

    public BMDefaultDatabase(Context context, String address) {
        super(context, address);
    }

    @Override
    public ContentValues addToValues(ContentValues values, String key, double data) {
        throw new RuntimeException("Cannot add to values with a default Blue Maestro database!");
    }

    @Override
    public DownloadState downloadData(UartService service, BMDownloader downloader, BMProgressIndicator progress) throws UnsupportedEncodingException {
        throw new RuntimeException("Cannot download data to a default Blue Maestro database!");
    }

    @Override
    public void displayAsChart(Chart chart, BMProgressIndicator progress) {
        throw new RuntimeException("Cannot display data as a chart from a default Blue Maestro database!");
    }

    @Override
    public void displayAsTable(Context context, ListView listView) {
        throw new RuntimeException("Cannot display data as a table from a default Blue Maestro database!");
    }

    @Override
    public File export(Context context, String filename) throws IOException {
        throw new RuntimeException("Cannot export data to a .csv file from a default Blue Maestro database!");
    }
}
