package com.bluemaestro.tempo_utility;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bluemaestro.tempo_utility.sql.BMPebbleDatabase27;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by iMac27 on 13/03/2017.
 */

public class CustomAdapter27 extends BaseAdapter {

    private Context context;
    private ArrayList<BMPebbleDatabase27.Readings> readings27;
    private LayoutInflater inflater;
    private String unitsForTemperature;
    private String unitsForHumidity;

    public CustomAdapter27(Context context, ArrayList<BMPebbleDatabase27.Readings>readings, String units) {
        this.context = context;
        this.readings27 = readings;
        this.unitsForTemperature = units;
        this.unitsForHumidity = "% RH";

    }

    @Override
    public int getCount() {
        return readings27.size();
    }

    @Override
    public Object getItem(int position) {
        return readings27.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (view == null) {
            view = inflater.inflate(R.layout.table_element, parent, false);
        }

        TextView logNumber = (TextView)view.findViewById(R.id.log_number);
        TextView timeStamp = (TextView)view.findViewById(R.id.log_timestamp);
        TextView temperatureValue = (TextView)view.findViewById(R.id.log_temperature_value);
        TextView humidityValue = (TextView)view.findViewById(R.id.log_humidity_value);
        TextView dewpointValue = (TextView)view.findViewById(R.id.log_dewpoint_value);
        TextView pressureValue = (TextView)view.findViewById(R.id.log_pressure_value);
        TextView divider = (TextView)view.findViewById(R.id.dividerfor27);

        logNumber.setVisibility(View.VISIBLE);
        timeStamp.setVisibility(View.VISIBLE);
        temperatureValue.setVisibility(View.VISIBLE);
        humidityValue.setVisibility(View.VISIBLE);
        dewpointValue.setVisibility(View.VISIBLE);
        pressureValue.setVisibility(View.VISIBLE);
        divider.setVisibility(View.VISIBLE);




        logNumber.setText(String.valueOf(readings27.get(position).getIndexValue()));
        timeStamp.setText(readings27.get(position).getDateStamp());
        if (unitsForTemperature.matches("º F")) {
            Float temperatureValueForDisplayNum = (float)((readings27.get(position).getTemparatureValue() * 1.8) + 32);
            String temperatureValueForDisplayString = String.format("%.1f", temperatureValueForDisplayNum);
            temperatureValue.setText(temperatureValueForDisplayString+" "+unitsForTemperature);

            Float dewpointValueForDisplayNum = (float)((readings27.get(position).getDewpointValue() * 1.8) + 32);
            String dewpointValueForDisplayString = String.format("%.1f", dewpointValueForDisplayNum);
            dewpointValue.setText(dewpointValueForDisplayString+" "+unitsForTemperature);
        } else {
            temperatureValue.setText(String.valueOf(readings27.get(position).getTemparatureValue()) + " " + unitsForTemperature);
            dewpointValue.setText(String.valueOf(readings27.get(position).getDewpointValue()) + " " + unitsForTemperature);
        }

        humidityValue.setText(String.valueOf(readings27.get(position).getHumidityValue()) + " " + unitsForHumidity);
        pressureValue.setText(String.valueOf(readings27.get(position).getPressureValue()) + " " + "hPa");
        return view;

    }




}