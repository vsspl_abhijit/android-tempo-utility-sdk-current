package com.bluemaestro.tempo_utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.bluemaestro.tempo_utility.devices.BMDeviceMap;
import com.bluemaestro.tempo_utility.sql.BMDatabase;
import com.bluemaestro.tempo_utility.views.dialogs.BMAlertDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Garrett on 16/08/2016.
 */
public class StoredDataActivity extends Activity {

    public static final String TAG = "StoredDataActivity";

    private TextView mEmptyList;
    private List<String> addressList;
    private DeviceAdapter deviceAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        View rootView = findViewById(android.R.id.content).getRootView();
        StyleOverride.setDefaultTextColor(rootView, Color.BLACK);
        StyleOverride.setDefaultBackgroundColor(rootView, Color.WHITE);
        StyleOverride.setDefaultFont(rootView, this, "Montserrat-Regular.ttf");

        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
        setContentView(R.layout.device_list);
        android.view.WindowManager.LayoutParams layoutParams = this.getWindow().getAttributes();
        layoutParams.gravity = Gravity.TOP;
        layoutParams.y = 200;

        mEmptyList = (TextView) findViewById(R.id.empty);
        mEmptyList.setText(R.string.searching);

        Button cancelButton = (Button) findViewById(R.id.btn_cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        populateList();

        searchForDatabases();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void populateList(){
        Log.d(TAG, "populateList");
        addressList = new ArrayList<String>();
        deviceAdapter = new DeviceAdapter(this, addressList);

        ListView newDevicesListView = (ListView) findViewById(R.id.new_devices);
        newDevicesListView.setAdapter(deviceAdapter);
        newDevicesListView.setOnItemClickListener(mDeviceClickListener);
    }

    private void searchForDatabases(){
        Log.d(TAG, "searchForDatabases");
        mEmptyList.setVisibility(View.GONE);
        String[] addresses = getApplicationContext().databaseList();
        for(String address : addresses) {
            if(address.contains("journal")) continue;
            addressList.add(address);
            //deviceAdapter.notifyDataSetChanged();
        }

    }

    private AdapterView.OnItemClickListener mDeviceClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Bundle b = new Bundle();
            b.putString(BluetoothDevice.EXTRA_DEVICE, addressList.get(position));
            Intent result = new Intent();
            result.putExtras(b);
            setResult(Activity.RESULT_OK, result);
            finish();
        }
    };

    private class DeviceAdapter extends BaseAdapter {
        Context context;
        List<String> addressList;
        LayoutInflater inflater;

        public DeviceAdapter(Context context, List<String> addressList) {
            this.context = context;
            inflater = LayoutInflater.from(context);
            this.addressList = addressList;
        }

        @Override
        public int getCount() {
            return addressList.size();
        }

        @Override
        public Object getItem(int position) {
            return addressList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewGroup vg = (convertView != null)
                    ? (ViewGroup) convertView
                    : (ViewGroup) inflater.inflate(R.layout.device_element, null);

            String address = addressList.get(position);
            final TextView tvname = (TextView) vg.findViewById(R.id.name);
            final TextView tvaddr = (TextView) vg.findViewById(R.id.address);
            final TextView tvcount = (TextView) vg.findViewById(R.id.data_count);
            final Button btndel = (Button) vg.findViewById(R.id.button_delete);

            if(address == null) return vg;
            Log.d(TAG, "Database name "+ address);
            final BMDatabase database = BMDeviceMap.INSTANCE.createReadableBMDatabase(context, address);
            String name = database.getName();
            if(database == null) return vg;
            // Name
            Log.d(TAG, "Database name "+ database.getName());

            tvname.setText(database.getName());
            tvname.setVisibility(View.VISIBLE);
            tvcount.setText("Number of records: "+database.getDataCount());
            tvcount.setVisibility(View.VISIBLE);
            database.close();
            // MAC address
            tvaddr.setText(address);
            tvaddr.setVisibility(View.VISIBLE);

            // Show delete button
            btndel.setVisibility(View.VISIBLE);
            btndel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            BMAlertDialog dialog = new BMAlertDialog(StoredDataActivity.this,
                                    "",
                                    "Delete database for " + database.getAddress() +
                                            "\n(" + database.getAddress() + ") ?");

                            dialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    addressList.remove(database.getAddress());
                                    StoredDataActivity.this.deleteDatabase(database.getAddress());
                                    deviceAdapter.notifyDataSetChanged();
                                }
                            });
                            dialog.setNegativeButton("NO", null);

                            dialog.show();
                            dialog.applyFont(StoredDataActivity.this, "Montserrat-Regular.ttf");
                        }
                    });
                }
            });

            return vg;
        }
    }
}
