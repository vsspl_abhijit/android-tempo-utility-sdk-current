package com.bluemaestro.tempo_utility.views.graphs;


import android.graphics.Canvas;
import android.graphics.Paint;

import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.ViewPortHandler;

/**
 * Created by Garrett on 02/09/2016.
 *
 * A zone to show the area of consideration around a limit.
 */
public class LimitZone {

    protected Paint zone;

    protected float threshold;
    protected String op;

    protected ViewPortHandler viewPortHandler;
    protected Transformer axisTransformer;
    protected int color;

    public LimitZone(float threshold, String op){
        this.zone = new Paint();
        this.threshold = threshold;
        this.op = op;
    }

    public void setViewPortHandler(ViewPortHandler viewPortHandler){
        this.viewPortHandler = viewPortHandler;
    }

    public void setAxisTransformer(Transformer axisTransformer){
        this.axisTransformer = axisTransformer;
    }

    public void setColor(int color){
        this.color = color;
    }

    public void onDraw(Canvas canvas){
        float[] points = new float[4];
        points[1] = threshold;
        axisTransformer.pointValuesToPixel(points);

        float top, bottom;
        if(op.equals(">") || op.equals(">=")){
            top = 76;
            bottom = points[1];
        } else if(op.equals("<") || op.equals("<=")){
            top = points[1];
            bottom = viewPortHandler.contentBottom();
        } else{
            top = points[1];
            bottom = points[1];
        }

        zone.setColor(color);
        canvas.drawRect(
                viewPortHandler.contentLeft(),
                top,
                viewPortHandler.contentRight(),
                bottom,
                zone
        );
    }
}
