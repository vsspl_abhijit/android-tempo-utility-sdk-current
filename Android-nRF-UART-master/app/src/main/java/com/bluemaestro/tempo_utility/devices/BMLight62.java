package com.bluemaestro.tempo_utility.devices;

import android.bluetooth.BluetoothDevice;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bluemaestro.tempo_utility.Log;
import com.bluemaestro.tempo_utility.R;
import com.bluemaestro.tempo_utility.ble.Utility;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.data.Entry;
import com.bluemaestro.tempo_utility.views.graphs.BMLineChart;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Garrett on 15/08/2016.
 */
public class BMLight62 extends BMDevice {

    private static final Pattern burst_pattern =
            Pattern.compile("x(-?[0-9]+)y(-?[0-9]+)z(-?[0-9]+)");

    private int battery;
    private int loggingInterval;
    private int globalLogCount;
    private int logPointer;
    private int referenceDateRawNumber;

    private byte mode;

    public BMLight62(BluetoothDevice device, byte id) {
        super(device, id);
    }
    public int getBatteryLevel() {
        return battery;
    }
    public int getLoggingInterval() {
        return loggingInterval;
    }
    public int getGlobalLogCount() {
        return globalLogCount;
    }
    public int getLogPointer() {
        return logPointer;
    }
    public String getReferenceDate() {
        Utility dateUtility = new Utility();
        return dateUtility.convertNumberIntoDate(String.valueOf(referenceDateRawNumber));
    }


    public String convertIntervalToDate(int loggingPeriod, int currentPeriod, int loggingInterval) {

        int seconds = ((currentPeriod - loggingPeriod) * loggingInterval);
        int day = (int)TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds) - (day *24);
        long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds)* 60);
        long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) *60);
        String timeAgo = "D:"+day+" H:"+hours+" M:"+minute+" S:"+second;
        return timeAgo;
    }


    @Override
    public void updateWithData(int rssi, String name, byte[] mData, byte[] sData){
        super.updateWithData(rssi, name, mData, sData);

        //Advertising Packet
        this.battery = mData[4];
        this.loggingInterval = convertToUInt16(mData[5], mData[6]);
        this.globalLogCount = convertToUInt16(mData[7], mData[8]);
        this.logPointer = convertToUInt16(mData[9], mData[10]);
        mode = 0;

        //Scan Response Packet
        this.referenceDateRawNumber = ((0xFF & sData[3]) << 24) | ((0xFF & sData[4]) << 16) |  ((0xFF & sData[5]) << 8) | (0xFF & sData[6]);
        Log.d("BMButton", "referenceDateNumber" + this.referenceDateRawNumber);

    }



    @Override
    public void updateViewGroup(ViewGroup vg){
        super.updateViewGroup(vg);
        final TextView tvbatt = (TextView) vg.findViewById(R.id.battery);

        final TextView tvLoggingInterval = (TextView) vg.findViewById(R.id.loggingInterval);
        final TextView tvButtonPressCountLabel = (TextView) vg.findViewById(R.id.push_button_label_v42);
        final TextView tvButtonPushCount = (TextView) vg.findViewById(R.id.push_button_value_v42);
        final TextView tvMode = (TextView) vg.findViewById(R.id.mode_42);


        // Set visible
        vg.findViewById(R.id.button_push_box_v42).setVisibility(View.VISIBLE);

        // Battery
        int battery = getBatteryLevel();
        if(battery >= 0 && battery <= 100){
            tvbatt.setVisibility(View.VISIBLE);
            tvbatt.setText("Battery: " + battery + "%");
        }
        //Mode
        tvMode.setText(""+getMode());
        tvMode.setVisibility(View.VISIBLE);


    }

    public void updateViewGroupForDetails(ViewGroup vg){

        //Device Information
        TextView name = (TextView) vg.findViewById(R.id.deviceName);
        TextView address = (TextView) vg.findViewById(R.id.deviceAddress);
        TextView battery = (TextView) vg.findViewById(R.id.batteryLevel);
        TextView signal = (TextView) vg.findViewById(R.id.rssiStrength);
        TextView referenceDate = (TextView) vg.findViewById(R.id.referenceDate);
        TextView lastDownloadDate = (TextView) vg.findViewById(R.id.lastDownloadDate);
        TextView version = (TextView) vg.findViewById(R.id.versionNumber);
        TextView currentLogCount = (TextView) vg.findViewById(R.id.loggingCount_42_main);
        TextView logIntervalSeconds = (TextView) vg.findViewById(R.id.loggingIntervalSeconds_42_main);
        TextView numberOfLogs = (TextView) vg.findViewById(R.id.number_of_logs);


        //Ever Recorded Values
        TextView everThresholdOneEvents = (TextView) vg.findViewById(R.id.thresholdOne_loggedEventsNo_value);

        //Set Values
        name.setText(getName());
        address.setText("Device ID: " + getAddress());
        battery.setText("Battery: " + getBatteryLevel() + "%");
        signal.setText("Signal: " + getRSSI() + "dB");
        version.setText("Version: " + getVersion());

        referenceDate.setText("Reference Date : " + getReferenceDate());
        lastDownloadDate.setVisibility(View.GONE);

        logIntervalSeconds.setText("Logging Interval: "+getLoggingInterval()+"s");
        numberOfLogs.setText("Number of Logs: "+getGlobalLogCount());

    }

    @Override
    public void setupChart(Chart chart, String command){
        if(chart instanceof BMLineChart && command.equals("*bur")){
            // If we sent the "*bur" command, setup the chart
            BMLineChart lineChart = (BMLineChart) chart;
            lineChart.init("", 5);
            lineChart.setLabels(
                    new String[]{"X", "Y", "Z"},
                    new int[]{Color.RED, Color.BLUE, Color.BLACK}
            );
        }
    }


    @Override
    public void updateChart(Chart chart, String text){
        if(chart instanceof BMLineChart){
            BMLineChart lineChart = (BMLineChart) chart;
            // Only continue if it's XYZ values
            Matcher matcher = burst_pattern.matcher(text);
            if(!matcher.matches()) return;

            // Parse values
            int[] value = new int[3];
            for(int i = 0; i < value.length; i++) {
                value[i] = new Integer(matcher.group(i + 1).trim());
            }
            // Insert values as new entries
            int index = lineChart.getEntryCount() / 3;
            lineChart.addEntry("X", new Entry(index, value[0]));
            lineChart.addEntry("Y", new Entry(index, value[1]));
            lineChart.addEntry("Z", new Entry(index, value[2]));
        }
    }
}
