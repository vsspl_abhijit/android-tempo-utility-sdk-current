package com.bluemaestro.tempo_utility.sql.downloading;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import com.bluemaestro.tempo_utility.Log;

import com.bluemaestro.tempo_utility.MainActivity;
import com.bluemaestro.tempo_utility.UartService;
import com.bluemaestro.tempo_utility.ble.Utility;
import com.bluemaestro.tempo_utility.sql.BMDatabase;
import com.bluemaestro.tempo_utility.views.dialogs.BMAlertDialog;
import com.bluemaestro.tempo_utility.views.dialogs.BMProgressIndicator;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 * Created by Garrett on 23/08/2016.
 *
 * Handles downloading data from the device
 */
public class BMDownloader extends BroadcastReceiver{

    private BMDatabase mBMDatabase;

    private int lastPointer;
    private int recordsNeeded;
    private int globalLogCount;
    private int sizePerRecord;
    private byte mode;

    private int thresholdNumber;
    private int[] thresholdTypes;
    private short[] thresholds;

    private int downloadCount;

    private volatile boolean downloading;
    private volatile boolean connected;
    private volatile boolean downloadFailure= true;
    private volatile boolean waitingForBluetooth = true;

    BMProgressIndicator progessToUpdate;

    private int type;
    private int index;
    private double[][] data;

    public BMDownloader(BMDatabase bmDatabase, BMProgressIndicator progress){
        this.mBMDatabase = bmDatabase;
        this.progessToUpdate = progress;
        clear();
    }


    public int getLastPointer(){
        return lastPointer;
    }
    public byte getMode() {
        return mode;
    }
    public int getDownloadCount() { return downloadCount; }
    public boolean isDownloading() {
        return downloading;
    }
    public boolean isConnected(){
        return connected;
    }
    public boolean isdownloadFailure() {return downloadFailure;}
    public boolean isWaitingForBluetooth() { return waitingForBluetooth;}
    public int getThresholdNumber(){
        return thresholdNumber;
    }
    public int[] getThresholdTypes(){
        return thresholdTypes;
    }
    public short[] getThresholds(){
        return thresholds;
    }
    public int getIndex() {return index;}

    public double[][] getData(){
        return Arrays.copyOfRange(data, 0, type + 1);
    }

    public void clear(){
        this.lastPointer = -1;
        this.recordsNeeded = -1;
        this.globalLogCount = -1;
        this.sizePerRecord = -1;
        this.mode = -1;

        this.downloadCount = 0;

        this.downloading = true;
        this.connected = true;
        this.waitingForBluetooth = true;

        this.thresholdNumber = 0;
        this.thresholdTypes = new int[thresholdNumber];
        this.thresholds = new short[thresholdNumber];

        this.type = 0;
        this.index = 0;
        this.data = new double[0][0];
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        //*********************//
        if (action.equals(UartService.ACTION_GATT_DISCONNECTED)) {
            Log.d("BMDownloader", "Disconnected");
            if (!isdownloadFailure()) {
                this.downloading = false;
                this.connected = false;
                mBMDatabase.completedDownload();
            }
        }

        //*********************//
        if (action.equals(UartService.UART_TX_ENABLED)) {
            waitingForBluetooth = false;
        }

        //*********************//
        if (action.equals(UartService.ACTION_DATA_AVAILABLE)) {
            final byte[] txValue = intent.getByteArrayExtra(UartService.EXTRA_DATA);
            //Log.d("BMDownloader", "Data: " + Utility.bytesAsHexString(txValue));


            if(txValue.length == 0) {
                Log.d("BMDownloader", "Length is zero");
                return;
            }

            if(txValue.length < 20 && txValue[txValue.length - 1] == ':'){
                this.lastPointer = convertToInt16(txValue[0], txValue[1]);
                this.recordsNeeded = convertToInt16(txValue[2], txValue[3]);
                this.globalLogCount = convertToInt16(txValue[4], txValue[5]);
                this.sizePerRecord = convertToInt16(txValue[6], txValue[7]);
                this.mode = txValue[8];

                this.thresholdNumber = (txValue[9] % 10 != 0 ? 1 : 0)
                                        + ((txValue[9] / 10) % 10 != 0 ? 1 : 0);
                this.thresholdTypes = new int[2];
                thresholdTypes[0] = txValue[9] % 10;
                thresholdTypes[1] = (txValue[9] / 10) % 10;
                this.thresholds = new short[2];
                thresholds[0] = ByteBuffer.wrap(Arrays.copyOfRange(txValue, 10, 12))
                        .order(ByteOrder.BIG_ENDIAN).getShort();
                thresholds[1] = ByteBuffer.wrap(Arrays.copyOfRange(txValue, 12, 14))
                        .order(ByteOrder.BIG_ENDIAN).getShort();

                Log.d("BMDownloader", "Threshold No.: " + thresholdNumber);
                Log.d("BMDownloader", "Threshold Type: " + thresholdTypes[0] + " | " + thresholdTypes[1]);
                Log.d("BMDownloader", "Threshold: " + thresholds[0] + " | Threshold: " + thresholds[1]);

                Log.d("BMDownloader", "Last Pointer: " + lastPointer +
                                        " | Records needed: " + recordsNeeded +
                                        " | Global Log Count: " + globalLogCount +
                                        " | Size per Record: " + sizePerRecord);
                this.downloadFailure = true;
                this.downloading = true;

                this.data = new double[10][recordsNeeded ];
            } else {
                for(int i = 0; i < txValue.length; i += sizePerRecord) {

                    if(txValue[i] == '.'){
                        this.downloadFailure = false;
                        this.downloading = false;
                        mBMDatabase.completedDownload();
                        Log.d("BMDownloader", "Came across terminator . ");
                        return;
                    } else if (((txValue[i] == ',') && (txValue[i+1] == ',')) || (this.index == recordsNeeded)) {
                        //Log.d("BMDownloader", "Came across separator , now changing type.  Type value is : " + this.type + " and new type value will be : " + (this.type + 1));
                        i = txValue.length;
                        this.index = 0;
                        this.type += 1;
                        continue;
                    }
                    byte[] record = Arrays.copyOfRange(txValue, i, i + sizePerRecord);
                    switch(sizePerRecord){
                        case 1:
                            // Byte
                            data[type][index] = record[0];
                            index ++;
                            progessToUpdate.setProgress(index);
                            break;
                        case 2:
                            // Int (as Short)
                            data[type][index] = ByteBuffer.wrap(record).order(ByteOrder.BIG_ENDIAN).getShort();
                            //Log.d("BMDownloader", "Index value is : " + index);
                            index ++;
                            progessToUpdate.setProgress(index);
                            break;
                        case 4:
                            // Float
                            data[type][index] = ByteBuffer.wrap(record).order(ByteOrder.BIG_ENDIAN).getFloat();
                            index ++;
                            progessToUpdate.setProgress(index);
                            break;
                        default:
                            return;

                    }
                }
            }
        }
    }

    /**
     * Convert two bytes to unsigned int 16
     * @param first
     * @param second
     * @return
     */
    protected static final int convertToInt16(byte first, byte second){
        int value = (int) first & 0xFF;
        value *= 256;
        value += (int) second & 0xFF;
        return value;
    }
}
