package com.bluemaestro.tempo_utility;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Garrett on 05/08/2016.
 */
public final class StyleOverride {

    private StyleOverride(){

    }
    public static void setDefaultFont(final View view, final Context context, String fontAsset){
        try{
            if(view instanceof ViewGroup){
                ViewGroup group = (ViewGroup) view;
                for(int i = 0; i < group.getChildCount(); i++){
                    View child = group.getChildAt(i);
                    setDefaultFont(child, context, fontAsset);
                }
            } else if(view instanceof TextView){
                TextView textView = (TextView) view;
                textView.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/" + fontAsset));
            }
        } catch(Exception exception){
            exception.printStackTrace();
        }
    }

    public static void setDefaultTextColor(final View view, int color){
        try{
            String name = view.toString();
            if(view instanceof ViewGroup){
                ViewGroup group = (ViewGroup) view;
                for(int i = 0; i < group.getChildCount(); i++){
                    View child = group.getChildAt(i);
                    setDefaultTextColor(child, color);
                }
            } else if(view instanceof TextView){
                TextView textView = (TextView) view;
                textView.setTextColor(color);
            }
        } catch(Exception exception){
            exception.printStackTrace();
        }
    }

    public static void setDefaultBackgroundColor(final View view, int color){
        try{
            String name = view.toString();
            if(view instanceof ViewGroup){
                ViewGroup group = (ViewGroup) view;
                for(int i = 0; i < group.getChildCount(); i++){
                    View child = group.getChildAt(i);
                    setDefaultBackgroundColor(child, color);
                }
            }
            view.setBackgroundColor(color);
        } catch(Exception exception){
            exception.printStackTrace();
        }
    }
}
