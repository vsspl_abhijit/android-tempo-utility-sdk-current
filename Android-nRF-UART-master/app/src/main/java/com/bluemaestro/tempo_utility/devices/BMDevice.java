package com.bluemaestro.tempo_utility.devices;

import android.bluetooth.BluetoothDevice;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.Chart;
import com.bluemaestro.tempo_utility.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Garrett on 05/08/2016.
 *
 * BMDevice is an abstract class for Blue Maestro BLE devices
 */
public abstract class BMDevice {

    /**
     * MAC address of the device
     */
    protected final String address;

    /**
     * Name of the device
     */
    protected String name;

    /**
     * ID/Version of the device
     */
    protected final byte version;

    /**
     * Received Signal Strength Indicator
     */
    protected byte rssi;

    /**
     * Bond state
     */
    protected int bondState;

    /**
     * Device mode
     */
    protected byte mode;

    /**
     * Whether logs can be downloaded from the device at this stage
     */
    protected boolean isDownloadReady;

    /**
     * Whether logs can be downloaded from the device at this stage
     */
    protected int logCount;

    /**
     * Constructor
     * @param device Bluetooth device
     * @param version Version of this Blue Maestro device
     */
    public BMDevice(BluetoothDevice device, byte version){
        this.address = device.getAddress();
        this.name = device.getName();
        this.version = version;
        this.bondState = device.getBondState();
        this.mode = 0;
    }

    public String getAddress() { return address; }
    public String getName() { return name; }
    public byte getVersion() { return version; }
    public byte getRSSI() { return rssi; }
    public int getBondState() {return bondState; }
    public byte getMode(){
        return mode;
    }
    public boolean isDownloadReady(){
        return isDownloadReady;
    }
    public int getLogCount() {return logCount; }

    public void setName(String name){
        this.name = name;
    }
    public void setDownloadReady(boolean isDownloadReady){
        this.isDownloadReady = isDownloadReady;
    }
    public void setMode(byte mode) {this.mode = mode;}

    /**
     * Updates this Blue Maestro device with new info
     * @param device
     */
    public void updateWithInfo(BluetoothDevice device){
        this.name = device.getName();
        this.bondState = device.getBondState();
    }

    /**
     * Updates this Blue Maestro device with new data
     * @param rssi The new RSSI of the device
     * @param mData The new manufacturer data of the device
     * @param sData The new scan response data of the device
     */
    public void updateWithData(int rssi, String name, byte[] mData, byte[] sData){
        this.rssi = (byte) rssi;
        this.name = name;
        this.logCount = convertToUInt16(mData[7], mData[8]);
    }

    public void processCommand(String command){
        Matcher matcher = Pattern.compile("\\*nam(.*)").matcher(command);
        if(matcher.matches()){
            this.name = matcher.group(1);
        }
    }

    /**
     * Sets up the chart for data
     * @param chart The chart to setup
     * @param command The command sent to the device
     */
    public abstract void setupChart(Chart chart, String command);

    /**
     * Updates the chart with data
     * @param chart The chart to update
     * @param text The data to update the chart with
     */
    public abstract void updateChart(Chart chart, String text);

    /**
     * Updates the device's view group to update its display in the device element list
     * @param vg The view group to update
     */
    public void updateViewGroup(ViewGroup vg){
        final TextView tvaddr = (TextView) vg.findViewById(R.id.address);
        final TextView tvname = (TextView) vg.findViewById(R.id.name);
        final TextView tvrssi = (TextView) vg.findViewById(R.id.rssi);

        // Name
        tvname.setText(getName());
        tvname.setVisibility(View.VISIBLE);

        // MAC address
        tvaddr.setText(getAddress());
        tvaddr.setVisibility(View.VISIBLE);

        // RSSI
        byte rssival = getRSSI();
        if (rssival != 0) {
            tvrssi.setVisibility(View.VISIBLE);
            tvrssi.setText("RSSI: " + String.valueOf(rssival) + " dBm");
        } else{
            tvrssi.setVisibility(View.GONE);
        }
    }

    public abstract void updateViewGroupForDetails(ViewGroup vg);

    public boolean equals(Object object){
        if(object == null) return false;
        if(!(object instanceof BMDevice)) return false;
        return address == ((BMDevice) object).address;
    }
    public int hashCode(){
        return address.hashCode();
    }

    /**
     * Checks if the scan data is from a Blue Maestro BLE device
     * @param data
     * @return
     */
    public static final boolean isBMDevice(byte[] data){
        return data.length >= 3
                && ((int) data[1] & 0xFF) == 0x33
                && ((int) data[2] & 0xFF) == 0x01;
    }

    /**
     * Convert two bytes to signed int 16
     * @param first
     * @param second
     * @return
     */
    protected static final int convertToInt16(byte first, byte second){
        int value = (int) first & 0xFF;
        value *= 256;
        value += (int) second & 0xFF;
        value -= (value > 32768) ? 65536 : 0;
        return value;
    }

    protected static final int convertToUInt16(byte first, byte second){
        int value = ((first & 0xff) << 8) | (second & 0xff);
        return value;
    }

    /**
     * Convert a byte to signed int 8
     * @param b
     * @return
     */
    protected static final int convertToUInt8(byte b){
        return (b >= 0) ? b : b + 256;
    }

    /**
     * Ensure an unsigned int 8 is treated correct
     * @param b
     * @return
     */
    protected static final int convertToInt8(byte b){
        int c = (int) (b & 0xff);
        return c;
    }





}

