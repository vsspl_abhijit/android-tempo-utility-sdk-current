package com.bluemaestro.tempo_utility.devices;

import android.bluetooth.BluetoothDevice;
import android.graphics.Color;
import com.bluemaestro.tempo_utility.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bluemaestro.tempo_utility.ble.Utility;
import com.bluemaestro.tempo_utility.sql.BMDatabase;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.data.Entry;
import com.bluemaestro.tempo_utility.R;
import com.bluemaestro.tempo_utility.views.graphs.BMLineChart;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Garrett on 17/08/2016.
 */
public class BMTempHumi22 extends BMDevice {

    private static final Pattern burst_pattern =
            Pattern.compile("T(-?[0-9]+\\.[0-9])H(-?[0-9]+\\.[0-9])D(-?[0-9]+\\.[0-9])");

    // Battery level
    private int battery;

    // Logging interval
    private int loggingInterval;

    // Current temperature, humidity, and dew point
    private double currTemperature;
    private double currHumidity;
    private double currDewPoint;

    // Number of threshold breaches
    private int numBreach;

    // Highest temperature and humidity recorded
    private double highTemperature;
    private double highHumidity;

    // Lowest temperature and humidity recorded
    private double lowTemperature;
    private double lowHumidity;

    // Highest temperature, humidity, and dew point recorded in last 24 hours
    private double high24Temperature;
    private double high24Humidity;
    private double high24DewPoint;

    // Lowest temperature, humidity, and dew point recorded in last 24 hours
    private double low24Temperature;
    private double low24Humidity;
    private double low24DewPoint;

    // Average temperature, humidity, and dew point recorded in last 24 hours
    private double avg24Temperature;
    private double avg24Humidity;
    private double avg24DewPoint;

    public BMTempHumi22(BluetoothDevice device, byte id) {
        super(device, id);
    }

    @Override
    public void updateWithData(int rssi, String name, byte[] mData, byte[] sData){
        super.updateWithData(rssi, name, mData, sData);
        this.battery = mData[4];

        this.loggingInterval = convertToInt16(mData[5], mData[6]);
        BMDatabase database = BMDeviceMap.INSTANCE.getBMDatabase(getAddress());
        database.setLoggingInterval(loggingInterval);

        this.currTemperature = convertToInt16(mData[9], mData[10]) / 10.0;
        this.currHumidity = convertToInt16(mData[11], mData[12]) / 10.0;
        this.currDewPoint = convertToInt16(mData[13], mData[14]) / 10.0;

        this.mode = mData[15];

        this.numBreach = convertToInt8(mData[16]);

        this.highTemperature = convertToInt16(sData[3], sData[4]) / 10.0;
        this.highHumidity = convertToInt16(sData[5], sData[6]) / 10.0;

        this.lowTemperature = convertToInt16(sData[7], sData[8]) / 10.0;
        this.lowHumidity = convertToInt16(sData[9], sData[10]) / 10.0;

        this.high24Temperature = convertToInt16(sData[11], sData[12]) / 10.0;
        this.high24Humidity = convertToInt16(sData[13], sData[14]) / 10.0;
        this.high24DewPoint = convertToInt16(sData[15], sData[16]) / 10.0;

        this.low24Temperature = convertToInt16(sData[17], sData[18]) / 10.0;
        this.low24Humidity = convertToInt16(sData[19], sData[20]) / 10.0;
        this.low24DewPoint = convertToInt16(sData[21], sData[22]) / 10.0;

        this.avg24Temperature = convertToInt16(sData[23], sData[24]) / 10.0;
        this.avg24Humidity = convertToInt16(sData[25], sData[26]) / 10.0;
        this.avg24DewPoint = convertToInt16(sData[27], sData[28]) / 10.0;
    }

    public int getBatteryLevel(){
        return battery;
}

    public int getLoggingInterval(){
        return loggingInterval;
    }

    public double getCurrentTemperature(){
        return currTemperature;
    }
    public double getCurrentHumidity(){
        return currHumidity;
    }
    public double getCurrentDewPoint(){
        return currDewPoint;
    }

    public boolean isInAeroplaneMode(){
        return mode % 100 == 5;
    }
    public boolean isInFahrenheit(){
        return mode >= 100;
    }
    public String getTempUnits(){
        return isInFahrenheit() ? "°F" : "°C";
    }

    public int getNumBreach(){
        return numBreach;
    }

    public double getHighestTemperature(){
        return highTemperature;
    }
    public double getHighestHumidity(){
        return highHumidity;
    }

    public double getLowestTemperature(){
        return lowTemperature;
    }
    public double getLowestHumidity(){
        return lowHumidity;
    }

    public double getHighest24Temperature(){
        return high24Temperature;
    }
    public double getHighest24Humidity(){
        return high24Humidity;
    }
    public double getHighest24DewPoint(){
        return high24DewPoint;
    }

    public double getLowest24Temperature(){
        return low24Temperature;
    }
    public double getLowest24Humidity(){
        return low24Humidity;
    }
    public double getLowest24DewPoint(){
        return low24DewPoint;
    }

    public double getAverage24Temperature(){
        return avg24Temperature;
    }
    public double getAverage24Humidity(){
        return avg24Humidity;
    }
    public double getAverage24DewPoint(){
        return avg24DewPoint;
    }

    @Override
    public void updateViewGroup(ViewGroup vg){
        super.updateViewGroup(vg);

        // Battery
        final TextView tvbatt = (TextView) vg.findViewById(R.id.battery);

        // Logging interval
        final TextView tvlogi = (TextView) vg.findViewById(R.id.loggingInterval);

        // Current Temperature
        final TextView tvtemplabel = (TextView) vg.findViewById(R.id.temperature_label);
        final TextView tvtempvalue = (TextView) vg.findViewById(R.id.temperature_value);

        //Current Humidity
        final TextView tvhumilabel = (TextView) vg.findViewById(R.id.humidity_label);
        final TextView tvhumivalue = (TextView) vg.findViewById(R.id.humidity_value);

        //Current Dewpoint
        final TextView tvdewplabel = (TextView) vg.findViewById(R.id.dewPoint_label);
        final TextView tvdewpvalue = (TextView) vg.findViewById(R.id.dewPoint_value);

        // Global
        /*
        final TextView tvtempHighLabel = (TextView) vg.findViewById(R.id.highTempLabel);
        final TextView tvtempHigh = (TextView) vg.findViewById(R.id.highTemp);
        final TextView tvtempLowLabel = (TextView) vg.findViewById(R.id.lowTempLabel);
        final TextView tvtempLow = (TextView) vg.findViewById(R.id.lowTemp);
        final TextView tvhumiHighLabel = (TextView) vg.findViewById(R.id.highHumiLabel);
        final TextView tvhumiHigh = (TextView) vg.findViewById(R.id.highHumi);
        final TextView tvhumiLowLabel = (TextView) vg.findViewById(R.id.lowHumiLabel);
        final TextView tvHumiLow = (TextView) vg.findViewById(R.id.lowHumi);

        // 24 hour
        final TextView tvlabel24High = (TextView) vg.findViewById(R.id.high24Label);
        final TextView tvtemp24High = (TextView) vg.findViewById(R.id.high24Temp);
        final TextView tvhumi24High = (TextView) vg.findViewById(R.id.high24Humi);
        final TextView tvdewp24High = (TextView) vg.findViewById(R.id.high24Dew);

        final TextView tvlabel24Low = (TextView) vg.findViewById(R.id.low24Label);
        final TextView tvtemp24Low = (TextView) vg.findViewById(R.id.low24Temp);
        final TextView tvhumi24Low = (TextView) vg.findViewById(R.id.low24Humi);
        final TextView tvdewp24Low = (TextView) vg.findViewById(R.id.low24Dew);

        final TextView tvlabel24Avg = (TextView) vg.findViewById(R.id.avg24Label);
        final TextView tvtemp24Avg = (TextView) vg.findViewById(R.id.avg24Temp);
        final TextView tvhumi24Avg = (TextView) vg.findViewById(R.id.avg24Humi);
        final TextView tvdewp24Avg = (TextView) vg.findViewById(R.id.avg24Dew);
        */
        // Mode
        final TextView tvmode = (TextView) vg.findViewById(R.id.mode);

        //Units
        final TextView tvunits = (TextView) vg.findViewById(R.id.units);

        // Number of breaches
        final TextView tvnumb = (TextView) vg.findViewById(R.id.numBreach);

        // Set visible
        vg.findViewById(R.id.temperature_box).setVisibility(View.VISIBLE);
        vg.findViewById(R.id.humidity_box).setVisibility(View.VISIBLE);
        vg.findViewById(R.id.dewpoint_box).setVisibility(View.VISIBLE);
        vg.findViewById(R.id.tables).setVisibility(View.VISIBLE);
        vg.findViewById(R.id.extras).setVisibility(View.VISIBLE);

        // Battery
        int battery = getBatteryLevel();
        if(battery >= 0 && battery <= 100){
            tvbatt.setVisibility(View.VISIBLE);
            tvbatt.setText("Battery: " + battery + "%");
        }

        // Logging interval
        tvlogi.setVisibility(View.VISIBLE);
        tvlogi.setText("Logging interval: " + getLoggingInterval() + " s");

        // Current Temperature
        tvtemplabel.setVisibility(View.VISIBLE);
        tvtemplabel.setText("TEMP");
        tvtempvalue.setVisibility(View.VISIBLE);
        tvtempvalue.setText("" + Utility.convertValueTo(
                String.valueOf(getCurrentTemperature()), getTempUnits()) + getTempUnits());

        // Current Humidity
        tvhumilabel.setVisibility(View.VISIBLE);
        tvhumilabel.setText("HUMIDITY");
        tvhumivalue.setVisibility(View.VISIBLE);
        tvhumivalue.setText("" + + getCurrentHumidity() + "%");

        // Current Dewpoint
        tvdewplabel.setVisibility(View.VISIBLE);
        tvdewplabel.setText("DEWPNT");
        tvdewpvalue.setVisibility(View.VISIBLE);
        tvdewpvalue.setText("" + Utility.convertValueTo(
                String.valueOf(getCurrentDewPoint()), getTempUnits()) + getTempUnits());


            /*
        // Global
        tvtempHighLabel.setVisibility(View.VISIBLE);
        tvtempHighLabel.setText("Highest T: ");
        tvtempHigh.setVisibility(View.VISIBLE);
        tvtempHigh.setText("" + Utility.convertValueTo(
                String.valueOf(getHighestTemperature()), getTempUnits()) + getTempUnits());

        tvtempLowLabel.setVisibility(View.VISIBLE);
        tvtempLowLabel.setText("Lowest T: ");
        tvtempLow.setVisibility(View.VISIBLE);
        tvtempLow.setText("" + Utility.convertValueTo(
                String.valueOf(getLowestTemperature()), getTempUnits()) + getTempUnits());

        tvhumiHighLabel.setVisibility(View.VISIBLE);
        tvhumiHighLabel.setText("Highest H: ");
        tvhumiHigh.setVisibility(View.VISIBLE);
        tvhumiHigh.setText("" + getHighestHumidity() + "%");

        tvhumiLowLabel.setVisibility(View.VISIBLE);
        tvhumiLowLabel.setText("Lowest H: ");
        tvHumiLow.setVisibility(View.VISIBLE);
        tvHumiLow.setText("" + getLowestHumidity() + "%");

        // 24 hour
        tvlabel24High.setVisibility(View.VISIBLE);
        tvlabel24High.setText("24Hr High");
        tvtemp24High.setVisibility(View.VISIBLE);
        tvtemp24High.setText("T: " + Utility.convertValueTo(
                String.valueOf(getHighest24Temperature()), getTempUnits()) + getTempUnits());
        tvhumi24High.setVisibility(View.VISIBLE);
        tvhumi24High.setText("H: " + getHighest24Humidity() + "%");
        tvdewp24High.setVisibility(View.VISIBLE);
        tvdewp24High.setText("D: " + Utility.convertValueTo(
                String.valueOf(getHighest24DewPoint()), getTempUnits()) + getTempUnits());

        tvlabel24Low.setVisibility(View.VISIBLE);
        tvlabel24Low.setText("24Hr Low");
        tvtemp24Low.setVisibility(View.VISIBLE);
        tvtemp24Low.setText("T: " + Utility.convertValueTo(
                String.valueOf(getLowest24Temperature()), getTempUnits()) + getTempUnits());
        tvhumi24Low.setVisibility(View.VISIBLE);
        tvhumi24Low.setText("H: " + getLowest24Humidity() + "%");
        tvdewp24Low.setVisibility(View.VISIBLE);
        tvdewp24Low.setText("D: " + Utility.convertValueTo(
                String.valueOf(getLowest24DewPoint()), getTempUnits()) + getTempUnits());

        tvlabel24Avg.setVisibility(View.VISIBLE);
        tvlabel24Avg.setText("24Hr Avg");
        tvtemp24Avg.setVisibility(View.VISIBLE);
        tvtemp24Avg.setText("T: " + Utility.convertValueTo(
                String.valueOf(getAverage24Temperature()), getTempUnits()) + getTempUnits());
        tvhumi24Avg.setVisibility(View.VISIBLE);
        tvhumi24Avg.setText("H: " + getAverage24Humidity() + "%");
        tvdewp24Avg.setVisibility(View.VISIBLE);
        tvdewp24Avg.setText("D: " + Utility.convertValueTo(
                String.valueOf(getAverage24DewPoint()), getTempUnits()) + getTempUnits());
    */
        // Mode
        String modeText = "Mode: " +
                (isInAeroplaneMode() ? "\nFlight" : "Normal");
        //tvmode.setVisibility(View.VISIBLE);
        tvmode.setText(modeText);

        //Units
        String unitsText = "Units: " +
                (isInFahrenheit() ? "Fahrenheit" : "Celsius") +
                " (" + getTempUnits() + ")";
        //tvunits.setVisibility(View.VISIBLE);
        tvunits.setText(unitsText);

        // Number of breaches
        tvnumb.setVisibility(View.VISIBLE);
        tvnumb.setText("No. of threshold breaches: " + getNumBreach());
    }

    public void updateViewGroupForDetails(ViewGroup vg){

        String unitsForTemperature;
        String unitsForHumidity = "%";
        if ((int)getMode()> 100) {
            unitsForTemperature = "º F";
        } else {
            unitsForTemperature = "º C";
        }

        //Device Information
        TextView name = (TextView) vg.findViewById(R.id.deviceName);
        TextView address = (TextView) vg.findViewById(R.id.deviceAddress);
        TextView battery = (TextView) vg.findViewById(R.id.batteryLevel);
        TextView signal = (TextView) vg.findViewById(R.id.rssiStrength);
        TextView referenceDate = (TextView) vg.findViewById(R.id.referenceDate);
        TextView lastDownloadDate = (TextView) vg.findViewById(R.id.lastDownloadDate);
        TextView version = (TextView) vg.findViewById(R.id.versionNumber);

        //Current Values
        TextView unitsForTemp = (TextView) vg.findViewById(R.id.unitsForTemp);
        TextView temperatureValue = (TextView) vg.findViewById(R.id.tempValue);
        TextView humidityValue = (TextView) vg.findViewById(R.id.humValue);
        TextView dewpointValue = (TextView) vg.findViewById(R.id.dewpValue);

        //Last 24 Hour Values
        TextView high24Temp = (TextView) vg.findViewById(R.id.high24TempValue);
        TextView avg24Temp = (TextView) vg.findViewById(R.id.avg24TempValue);
        TextView low24Temp = (TextView) vg.findViewById(R.id.low24TempValue);
        TextView high24Humi = (TextView) vg.findViewById(R.id.high24HumValue);
        TextView avg24Humi = (TextView) vg.findViewById(R.id.avg24HumValue);
        TextView low24Humi = (TextView) vg.findViewById(R.id.low24HumValue);
        TextView high24Dewp = (TextView) vg.findViewById(R.id.high24DewpValue);
        TextView avg24Dewp = (TextView) vg.findViewById(R.id.avg24DewpValue);
        TextView low24Dewp = (TextView) vg.findViewById(R.id.low24DewpValue);

        //Last Forever values
        TextView highestTemp = (TextView) vg.findViewById(R.id.highestTempValue);
        TextView lowestTemp = (TextView) vg.findViewById(R.id.lowestTempValue);
        TextView highestHumi = (TextView) vg.findViewById(R.id.highestHumValue);
        TextView lowestHumi = (TextView) vg.findViewById(R.id.lowestHumValue);

        //Set Values
        name.setText(getName());
        address.setText("Device ID" + getAddress());
        battery.setText("Battery : " + getBatteryLevel() + "%");
        signal.setText("Signal : " + getRSSI() + "dB");
        version.setText("Version : " + getVersion());

        unitsForTemp.setText("Celsius");
        temperatureValue.setText("" + getCurrentTemperature() + unitsForTemperature);
        humidityValue.setText("" + getCurrentHumidity() + unitsForHumidity);
        dewpointValue.setText("" + getCurrentDewPoint() + unitsForTemperature);

        high24Temp.setText("" + getHighest24Temperature() + unitsForTemperature);
        avg24Temp.setText("" + getAverage24Temperature() + unitsForTemperature);
        low24Temp.setText("" + getLowest24Temperature() + unitsForTemperature);

        high24Humi.setText("" + getHighest24Humidity() + unitsForHumidity);
        avg24Humi.setText("" + getAverage24Humidity() + unitsForHumidity);
        low24Humi.setText("" + getLowest24Humidity() + unitsForHumidity);

        String highDewpForDisplay = String.format("%.1f", getHighest24DewPoint());
        String avgDewpForDisplay = String.format("%.1f", getAverage24DewPoint());
        String lowDewpForDisplay = String.format("%.1f", getLowest24DewPoint());

        high24Dewp.setText(highDewpForDisplay + unitsForTemperature);
        avg24Dewp.setText(avgDewpForDisplay + unitsForTemperature);
        low24Dewp.setText(lowDewpForDisplay + unitsForTemperature);

        highestTemp.setText("" + getHighestTemperature() + unitsForTemperature);
        lowestTemp.setText("" + getLowestTemperature() + unitsForTemperature);

        highestHumi.setText("" + getHighestHumidity() + unitsForHumidity);
        lowestHumi.setText("" + getLowestHumidity() + unitsForHumidity);


    }

    @Override
    public void setupChart(Chart chart, String command){
        if(!(chart instanceof BMLineChart)) return;
        BMLineChart lineChart = (BMLineChart) chart;
        if(command.equals("*bur")){
            isDownloadReady = !isDownloadReady;
            // If we sent the "*bur" command, setup the chart
            lineChart.init("", 5);
            lineChart.setLabels(
                    new String[]{
                            "Temperature (" + getTempUnits() + ")",
                            "Humidity",
                            "Dew Point (" + getTempUnits() + ")"
                    },
                    new int[]{
                            Color.RED,
                            Color.BLUE,
                            Color.GREEN
                    }
            );
        } else if(Pattern.matches("\\*units[c|f]", command)){
            // If we sent the "*units" command, change units
            boolean change = true;
            String oldUnits = getTempUnits();
            switch(command.charAt(command.length() - 1)){
                case 'c':   if(isInFahrenheit()) mode -= 100;   break;
                case 'f':   if(!isInFahrenheit()) mode += 100;  break;
                default:    change = false;                     break;
            }
            if(!change) return;
            int length = lineChart.getEntryCount() / 3;
            float[] temperature = new float[length];
            float[] humidity = new float[length];
            float[] dewPoint = new float[length];
            Log.d("BMTempHumi", "Length: " + length);
            // Store the old temperatures
            for(int i = 0; i < length; i++){
                temperature[i] = lineChart.getEntry("Temperature (" + oldUnits + ")", i).getY();
                humidity[i] = lineChart.getEntry("Humidity", i).getY();
                dewPoint[i] = lineChart.getEntry("Dew Point (" + oldUnits + ")", i).getY();
            }
            // Manipulate the graph
            lineChart.setLabels(
                    new String[]{
                            "Temperature (" + getTempUnits() + ")",
                            "Humidity",
                            "Dew Point (" + getTempUnits() + ")"
                    },
                    new int[]{
                            Color.RED,
                            Color.BLUE,
                            Color.GREEN
                    }
            );
            // Manipulate the data
            for (int i = 0; i < length; i++) {
                temperature[i] = (isInFahrenheit())
                        ? temperature[i] * 1.8f + 32.0f
                        : (temperature[i] - 32.0f) / 1.8f;
                dewPoint[i] = (isInFahrenheit())
                        ? dewPoint[i] * 1.8f + 32.0f
                        : (dewPoint[i] - 32.0f) / 1.8f;
            }
            // Add the temperatures back in
            for(int i = 0; i < length; i++){
                lineChart.addEntry("Temperature (" + getTempUnits() + ")", new Entry(i, temperature[i]));
                lineChart.addEntry("Humidity", new Entry(i, humidity[i]));
                lineChart.addEntry("Dew Point (" + getTempUnits() + ")", new Entry(i, dewPoint[i]));
            }
        } else if(Pattern.matches("\\*lint([0-9]+)", command)){
            Matcher matcher = Pattern.compile("\\*lint([0-9]+)").matcher(command);
            matcher.matches();
            int newLoggingInterval = Integer.valueOf(matcher.group(1).trim());
            BMDatabase database = BMDeviceMap.INSTANCE.getBMDatabase(getAddress());
            if(loggingInterval != newLoggingInterval){
                database.clear();
            }
            loggingInterval = newLoggingInterval;
            database.setLoggingInterval(loggingInterval);
        }
    }

    @Override
    public void updateChart(Chart chart, String text){
        if(!(chart instanceof BMLineChart)) return;
        BMLineChart lineChart = (BMLineChart) chart;
        // Only continue if it's THD values
        Matcher matcher = burst_pattern.matcher(text);
        if(!matcher.matches()) return;

        // Parse values
        float[] value = new float[3];
        for(int i = 0; i < value.length; i++) {
            value[i] = new Float(matcher.group(i + 1).trim());
        }
        // Insert values as new entries
        int index = lineChart.getEntryCount() / 3;
        lineChart.addEntry("Temperature (" + getTempUnits() + ")", new Entry(index, value[0]));
        lineChart.addEntry("Humidity", new Entry(index, value[1]));
        lineChart.addEntry("Dew Point (" + getTempUnits() + ")", new Entry(index, value[2]));
    }
}
