package com.bluemaestro.tempo_utility.views.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Button;
import android.widget.TextView;

import com.bluemaestro.tempo_utility.R;
import com.bluemaestro.tempo_utility.StyleOverride;
import com.bluemaestro.tempo_utility.views.generic.BMTextView;

/**
 * Created by Garrett on 18/08/2016.
 */
public class BMAlertDialog {

    private final AlertDialog.Builder builder;
    private AlertDialog dialog;

    public BMAlertDialog(Context context, String title, String message){
        final BMTextView tvtitle = new BMTextView(context);
        if (title == "") title = "Tempo Utility";
        tvtitle.setText(title);
        tvtitle.setTextSize(18);
        tvtitle.setPadding(30, 20, 20, 20);
        final BMTextView tvmessage = new BMTextView(context);
        tvmessage.setText(message);
        tvmessage.setTextSize(15);
        tvmessage.setPadding(30, 20, 20, 40);
        this.builder = new AlertDialog.Builder(context, R.style.DialogLight)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setCustomTitle(tvtitle)
            .setView(tvmessage);
    }

    public void setPositiveButton(String message, DialogInterface.OnClickListener clickListener){
        this.builder.setPositiveButton(message, clickListener);
    }

    public void setNegativeButton(String message, DialogInterface.OnClickListener clickListener){
        this.builder.setNegativeButton(message, clickListener);
    }

    public void setOnCancelListener(DialogInterface.OnCancelListener onCancelListener){
        this.builder.setOnCancelListener(onCancelListener);
    }

    public void show(){
        if (dialog != null) { //dialog.dismiss();

        }
        this.dialog = builder.show();
    }

    public void dismiss(){
        if(dialog != null){
            dialog.dismiss();
            dialog = null;
        }
    }

    public void updateMessage(Context context, String title, String message) {
        final BMTextView tvmessage = new BMTextView(context);
        tvmessage.setText(message);
        tvmessage.setTextSize(15);
        tvmessage.setPadding(30, 20, 20, 40);
        this.dialog.setView(tvmessage);

    }

    public void applyFont(Context context, String typeface){
        if(dialog == null) return;
        TextView message = (TextView) dialog.findViewById(android.R.id.message);
        Button yes = dialog.getButton(Dialog.BUTTON_POSITIVE);
        Button no = dialog.getButton(Dialog.BUTTON_NEGATIVE);

        StyleOverride.setDefaultFont(message, context, typeface);
        if(yes != null){
            StyleOverride.setDefaultFont(yes, context, typeface);
            yes.setTextSize(17);
        }
        if(no != null) {
            StyleOverride.setDefaultFont(no, context, typeface);
            no.setTextSize(17);
        }
    }

    public void preventCancelOnClickOutside() {
        if (dialog != null) {
            dialog.setCanceledOnTouchOutside(false);
        }
    }


}
